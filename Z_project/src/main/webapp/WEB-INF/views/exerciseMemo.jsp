<%
  /**
  * @Class Name : exerciseMemo.jsp
  * @Description : 후기 게시판  화면
  * @Modification Information
  *
  *   수정일              수정자                   수정내용
  *  -------    --------    ---------------------------
  *  2018.07.02 진경용                    글 자세히 보기 버튼 생성 및 활성화
  *
  * author Jin Kyung Yong
  * since 2018.06.01
  *
  * Copyright (C) 2018 by JKY  All right reserved.
  */
%>

<%@ page contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<HTML lang="ko-kr">
<head>
<meta charset="utf-8" />
<title>후기창.</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>무제 문서</title>
<!--select 용 스타일-->
<style>
select {
	width: 100px;
	height: 31px;
	padding-left: 10px;
	font-size: 18px;
	color: #006fff;
	border-radius: 3px;
}
</style>

<!-- 페이징용 스타일-->
<style>
.pagination {
	display: inline-block;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
	border: 1px solid #ddd;
}

.pagination a.active {
	background-color: #4CAF50;
	color: white;
	border: 1px solid #4CAF50;
}
.pagination a :hover:not(.active)
{background-color:#ddd;
}

.pagination a:first-child {
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
}

.pagination a:last-child {
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
}
</style>

<style>
.logo {
	width: 500px;
	margin-left: 45%
}

</style>


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!--jquery:3.2.0  -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<!--jquery-ui:1.12.1  -->	
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- bootstrap: 3.3.7-->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- angularjs: 1.6.9-->
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>

<!-- 글쓰기 화면 활성화 스크립트 -->
<script language="javascript">
	
	function openWriting(){
		window.open("${pageContext.request.contextPath}/writingExerciseMemo","writingExerciseMemo"
				,"width=1000,height=350,toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes,left=50,right=50");
		}
</script>
<!-- 글쓰기 화면 활성화 스크립트 /-->    
 
<!-- 게시판 글 자세히 보기 버튼 스크립트 --> 
<script>
	 function showDetail(boardNum){
		 
		 $.ajax({

             url : "${pageContext.request.contextPath}/showDetailService?boardNum="+boardNum,

            contentType : "application/json",

            success : function (json) {
                 // 주의사항 ! JSON.stringify() 사용 !	
                 var jsonObj = JSON.parse(JSON.stringify(json));

              	 //dialog박스 내용 설정
              	 $("#id").text(jsonObj.id);
              	 $("#boardDate").text(jsonObj.boardDate);
              	 $("#boardContent").text(jsonObj.boardContent);

                 $("#dialog").dialog();

            }, 

            error : function(xhr, status) {
                alert(xhr+" : "+status);
           } 

	}); // $.ajax
		 	
	 }
 </script>
 <!-- 게시판 글 자세히 보기 버튼 스크립트/ -->
    
 <!-- 로그인 이벤트 스크립트 -->
<script language="javascript">
	$(document).ready(function() {
		
		 //메인 화면 초기 상태:로그 아웃 메뉴 은닉
		 $("#logout_menu").hide(); 
		 $("#login_menu").show();
		
		 //btnLogin(로그인 버튼)  
		 $('#btnLogin').click(function() {
			 //로그아웃 버튼 은닉.
			 $("#logout_menu").hide();
			 
			 $.ajax({
				 url : 'http://localhost:8383/Z_project/login/logincheck',
				 type : 'get',
				 dataType:'text',
				 data : {
					 id : $('#id').val(),
					 pw : $('#pw').val()
				 }, // data 
				 success : function(data) {
					 alert('데이터 전송 test:'+data);
					 if(data.trim()!=''){
						 
						 //로그인 성공 후, 메인 화면의 메시지 출력ex)진경용님이 로그인 헀습니다.						 
						 $("#afterLogin").html(data+'님이 로그인 했습니다.');
						 
			 			 //로그인 메뉴 은닉.로그아웃 메뉴 등장.
						 $("#logout_menu").show();
						 $("#login_menu").hide();
						 alert("로그인에 성공하셨습니다.");
						 
					 }else{
						 alert("로그인 실패");
					 }
					 
			 	} // success
			 }) // ajax
		 }); //btnLogin(로그인 버튼)/
		 		 
		 //logout_menu(로그 아웃)
		 $("#logout_menu").click(function(){
			 
			 //로그인 버튼 은닉
			 $("#login_menu").hide();
				
				//세션 종료
				 $("#logout_menu").attr("data-target","#myModal2");
				  
				 //시작
				 $.ajax({
					 url : 'http://localhost:8383/Z_project/login/logout',
					 type : 'get',
					 dataType:'text',
					 data : {
						 id : $('#id').val()
					 }, // data 
					 success : function(data) {
						 alert('로그아웃 데이터:'+data);
						 if(data.trim()=='true'){
							 alert('로그아웃 성공');
							 $("#afterLogin").html('좋은 하루되세요');

							 //로그인 메뉴 등장.로그아웃 메뉴 은닉
							 $("#login_menu").show();
							 $("#logout_menu").hide();

						 }

					 }
				 });
				 //끝
		 });
	});//
	//logout_menu(로그 아웃)
</script>
<!-- 로그인 이벤트 스크립트/ -->   
    
</head>

<body>

<!-- 글 상세보기 -->
<div id="dialog" name="dialog" title="글 상세보기">
	날짜 :<section id="boardDate"> </section>
	작성자:<section id="id"></section>
	내용:<section id="boardContent"> </section>

</div>	
<!-- 글 상세보기 /-->

<div>
<!-- 게시판 인자 -->
총게시글 수:${pageInfo.listCount}<br/>
현재 페이지 :${pageInfo.page}<br/>
총 생성되는 페이지 :${pageInfo.maxPage}<br/>
시작 페이지:${pageInfo.startPage}<br/>
끝 페이지:${pageInfo.endPage}<br/>
</div>

<br/>
<div>
	<c:forEach items="${boardList}" var="board" varStatus="st">
		${st.count}, ${board.boardNum},${board.boardTitle},${board.boardDate},${board.boardReadCount}<br/>
	</c:forEach>
</div> 

<!------------------------------------- 운영자 연락처 안내 시작 ------------------------------------->
	<div align="right">
		<!-- 로그인 세션정보 출력 -->
		message: ${msg}<br>
	       세션정보:${sessionScope.LOGIN_SESS}<br>
		
		<div id="afterLogin">좋은 하루 되세요.</div>
		
		<!-- 로그인 세션정보 출력/ -->
	
		<span>
			<img src="image/top/naver.jpg" width="25px" height="25px"><a href="#">네이버</a>
			<img src="image/top/facebook.jpg" width="25px" height="25px"><a href="#">페이스 북</a>
			<img src="image/top/kakao.jpg" width="25px" height="25px">
			<span style="text-decoration: blink">id=ruddydwls</span>&nbsp;&nbsp;
		</span>
	</div>
<!-------------------------------------- 운영자 연락처 안내 /-------------------------------------->


<!-------------------------------------navbar시작 -------------------------------------------->
	<div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
			
				<!-- 홈페이지 main제목-->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
					</button>
					<a class="navbar-brand" href="홈페이지 임시 디자인.html">home(추후 이미지 삽입)</a>
				</div>
				<!-- 홈페이지 main제목/-->

				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">HOME</a></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동 정보<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="insertExercise">운동정보</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동기구<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="운동기구 리스트창.html">운동기구</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">보충제<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="보충제 제품 리스트창.html">보충제 </a></li>
							</ul></li>
							
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">게시판<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="insertMemoBoard">게시판 </a></li>
							</ul></li>	
							

					</ul>
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" />
						</div>
						<button type="submit" class="btn btn-default">검색</button>

					</form>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="이벤트창.html"><span
								class="glyphicon glyphicon-list">이벤트</span></a></li>
						<li><a href="회원가입.html "><span
								class="glyphicon glyphicon-user">회원가입</span></a></li>
								
								<!-- 로그인 시작 -->
						<li><a id="login_menu" data-toggle="modal" data-target="#myModal2"><span
								class="glyphicon glyphicon-user" id="login_title">로그인</span></a></li>
								<!-- 로그인 // -->
								
								<!-- 로그 아웃 시작 -->
						<li><a id="logout_menu">
							<span class="glyphicon glyphicon-user" id="login_title">로그아웃</span></a>
						</li>	
								<!-- 로그 아웃// -->
									
						<li><a href="#"><span
								class="glyphicon glyphicon-shopping-cart">쇼핑카트</span></a></li>
						<li><a data-toggle="modal" href="#myModal"><span
								class="glyphicon glyphicon-user">내정보</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>


<!--------------------------------------navbar/ --------------------------------------------->


<!--------------------------------------후기창 게시판 --------------------------------------------->

	<div class="logo">
		<h3>후기창 게시판입니다.</h3>
	</div>

	<!-- 테이블 시작-->
	<div class="container">
		<div>
			<!-- 내 글 확인하기 버튼-->
			<span style="float: right"><input type="button"
				value="내 글 확인하기" /> </span>
		</div>
		<!-- 게시판 글  -->
		<c:if test="${not empty boardList and pageInfo.listCount>0}">
			<table class="table table-condensed">
				<thead>
					<tr>
						<th style="width: 10%">NO</th>
						<th style="width: 50%">제목</th>
						<th style="width: 10%">사용자</th>
						<th style="width: 20%">DATE</th>
						<th style="width: 10%">조회</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${boardList}" var="board" varStatus="st">
						<tr>
							<td>${board.boardNum}</td>

							<td>${board.boardKind} ${board.boardTitle} <a
								href="${pageContext.request.contextPath}/testList/boardNum/${board.boardNum}/page/${pageInfo.page}/"
								data-toggle="modal" data-target="#boardView">
									<button type="button" class="btn btn-primary btn-xs"
										name="showDetail" id="showDetail"
										onclick="showDetail(${board.boardNum})">상세보기</button>
							</a></td>
							<td>${board.id}</td>
							<td>${board.boardDate}</td>
							<td>${board.boardReadCount}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<!-- 게시판 글 / -->


			<!--  페이징 시작 -->
			<section>
				<ul class="pagination">

					<!-- 처음으로 paging -->
					<c:choose>
						<c:when test="${pageInfo.page<=1}">
							<!-- 주의) 이 부분에서 bootstrap 페이징 적용시 불가피하게 <a> 기입. <a>없으면 적용 안됨. -->
							<li><a
								href="${pageContext.request.contextPath}/testList/page/1/limit/5">
									처음으로</a></li>
						</c:when>

						<c:otherwise>
							<li><a
								href="${pageContext.request.contextPath}/testList/page/1/limit/5">
									처음으로</a></li>
						</c:otherwise>
					</c:choose>
					<!-- 처음으로 paging/ -->
					
					<!-- 이전으로 paging -->
					<c:choose>
						<c:when test="${pageInfo.page<=1}">
							<!-- 주의) 이 부분에서 bootstrap 페이징 적용시 불가피하게 <a> 기입. <a>없으면 적용 안됨. -->
							<li><a
								href="${pageContext.request.contextPath}/testList/page/1/limit/5">
									이전</a></li>
						</c:when>

						<c:otherwise>
							<li><a
								href="${pageContext.request.contextPath}/testList/page/${pageInfo.page-1}/limit/5">
									이전</a></li>
						</c:otherwise>
					</c:choose>
					<!-- 이전으로 paging/ -->
					
					<!-- 페이지 지정으로 이동 -->
					<c:forEach var="a" begin="${pageInfo.startPage}"
						end="${pageInfo.startPage}">
						<c:choose>
							<c:when test="${a == pageInfo.page}">
								<li class="active"><a
									href="${pageContext.request.contextPath}/testList/${pageInfo.page}">${pageInfo.page}</a></li>
								<li><a
									href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+1}/limit/5">${pageInfo.page+1}</a></li>
								<li><a
									href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+2}/limit/5">${pageInfo.page+2}</a></li>
							</c:when>
							<c:otherwise>
								<li class="active"><a
									href="${pageContext.request.contextPath}/testList/page/${pageInfo.page}/limit/5">${pageInfo.page}</a></li>
								<li><a
									href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+1}/limit/5">${pageInfo.page+1}</a></li>
								<li><a
									href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+2}/limit/5">${pageInfo.page+2}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<!-- 페이지 지정으로 이동/-->

					<!-- 다음으로 paging -->
					<c:choose>
						<c:when test="">
							<li><a
								href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+1}/limit/5">
									다음</a></li>
						</c:when>

						<c:otherwise>
							<li><a
								href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+1}/limit/5">
									다음</a></li>
						</c:otherwise>
					</c:choose>
					<!-- 다음으로 paging/ -->
				</ul>
			</section>
		</c:if>

		<!-- 등록글 없을 경우 메시지 띄우기. -->
		<c:if test="${empty boardList || pageInfo.listCount==0}">
			<c:choose>
				<c:when test="${empty boardList || pageInfo.listCount==0}">
					<!-- 주의) 이 부분에서 bootstrap 페이징 적용시 불가피하게 <a> 기입. <a>없으면 적용 안됨. -->
					<section id="emptyArea">등록된 글이 없습니다.</section>
					<a
						href="${pageContext.request.contextPath}/testList/page/${pageInfo.page-1}/limit/5">
						<button type="button" class="btn btn-info">이전</button>
					</a>
				</c:when>
			</c:choose>
		</c:if>

		<!--버튼 및 검색 창 구현하기-->
		<div style="float: right">
			<form class="navbar-form navbar-left" action="/action_page.php">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search" /> <select>
						<option value="검색">검색</option>
						<option value="제목">제목</option>
						<option value="내용">내용</option>
						<option value="작성자">작성자</option>
					</select>
					<button type="submit" class="btn btn-default">검색</button>
					<input type="button" class="btn btn-default"
						onclick="openWriting()" value="글쓰기" />
				</div>
			</form>
		</div>
	</div>
	<!--------------------------------------후기창 게시판/ --------------------------------------------->
    
</body>
</html>
