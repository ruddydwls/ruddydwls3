<%
  /**
  * @Class Name : writingExercise.jsp
  * @Description : 신규 운동 등록 작성 화면
  * @Modification Information
  *
  *   수정일                수정자                   수정내용
  *  -------     --------    ---------------------------
  *  2018.07.04  진경용                   최초 생성
  *
  * author Jin Kyung Yong
  * since 2018.06.01
  *
  * Copyright (C) 2018 by JKY  All right reserved.
  */
%>
<%@ page contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko-kr">
<head>
<meta charset="utf-8" />
<title>게시판 글 쓰기</title>

<!-- jQuery : 3.2.0 -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<!-- submit버튼 클릭 후 스크립트 -->	
<script language="javascript">
  $(document).ready(function(){
   	 $("#submit").click(function() {
   		var form =$('form#writingExerciseForm')[0]; 	 	
   	 	var formData= new FormData(form);
		
   	 	$.ajax({
   	 		
 			cashe: false,
   	 		async:false,
   	 		contentType:false,
   	 		processData:false,
    	 		
 			type : 'post', 
 			url : '${pageContext.request.contextPath}/writingExercise.do', 
 			data : formData,
 			//참조:인자를 1개씩 넘길 경우: data: {id:$("#exerciseImgS").val()},"하나넘길떄"
 			
 			success : function()
 			{
 				alert("운동 등록 성공!!");
 				window.close();
 			}

 			
 		}); 
 	}); 
 });  
</script>
<!-- submit버튼 클릭 후 스크립트 /-->	
</head>

<!-- 운동 등록  -->
<body>
	<form id="writingExerciseForm" name="writingExerciseForm"
		action="${pageContext.request.contextPath}/writingExercise.do"
		method="post">
		<!--파일 전송시: enctype="mutipart/form-data" 추가-->
		<div>
			<!--운동 이름-->
			<div>
				<label>운동이름:</label> <input type="text" style="width: 300px;"
					name="exerciseName" id="exerciseName" />
			</div>
			<!--운동 이름/-->

			<!--운동 첫번째 사진-->
			<div>
				<label>운동 첫번째:</label> <input type="text" style="width: 300px;"
					name="exerciseImgF" id="exerciseImgF" />
			</div>
			<!--운동 첫번째 사진/-->

			<!--운동 두번째 사진-->
			<div>
				<label>운동 두번째:</label> <input type="text" style="width: 300px;"
					name="exerciseImgS" id="exerciseImgS" />
			</div>
			<!--운동 두번쨰 사진/-->

			<!--운동 세번째 사진-->
			<div>
				<label>운동 세번째:</label> <input type="text" style="width: 300px;"
					name="exerciseImgT" id="exerciseImgT" />
			</div>
			<!--운동 세번째 사진/-->

			<!--운동 썸네일 이미지-->
			<div>
				<label>운동 썸네일:</label> <input type="text" style="width: 300px;"
					name="thumbNail" id="thumbNail" />
			</div>
			<!--운동 썸네일 이미지/-->

			<!--운동 설명-->
			<div>
				<label>운동 설명:</label>
			</div>
			<textarea rows="4" cols="50" name="explain" id="explain"></textarea>
			<!--운동 설명/-->

			<!--운동 동영상 URL-->
			<div>
				<label>운동 동영상 주소:</label> <input type="text" style="width: 300px;"
					name="exerciseUrl" id="exerciseUrl" />
			</div>
			<!--운동 동영상 URL/-->
			
			<input type="button" id="submit" name="submit" value="글쓰기">
		</div>
	</form>
<!-- 운동 등록  /-->
</body>
</html>