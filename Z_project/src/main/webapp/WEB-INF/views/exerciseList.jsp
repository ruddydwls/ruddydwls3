<%
  /**
  * @Class Name : exerciseList.jsp
  * @Description : 운동  화면
  * @Modification Information
  *
  *   수정일                    수정자                   수정내용
  *  -------      --------    ---------------------------
  *  2018.07.05    진경용                    최초 생성
  *
  * author Jin Kyung Yong
  * since 2018.06.01
  *
  * Copyright (C) 2018 by JKY  All right reserved.
  */
%>

<%@ page contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="ko-kr"> 
<head>
<meta charset="utf-8" />
<title>홈페이지 임시 디자인.</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- 스타일 시트 -->
<style>
#wrapbox {
	direction: rtl;
}

#exercise_param{
height:0;
background-color: cyan;
overflow: hidden;
}
</style>

<link rel="stylesheet"
	href="<c:url value='/js/bootstrap/3.3.7/css/bootstrap.min.css'/>" />
<!-- jQuery:3.3.1	 -->
<script
	src="<c:url value='/js/jQuery/3.3.1/jquery-3.3.1.min.js'/>"></script>	
<!-- bootstrap:3.3.7 -->
<script
	src="<c:url value='/js/bootstrap/3.3.7/js/bootstrap.min.js' />"></script>	
<!-- angularJs:1.6.10 -->
<script
	src="<c:url value='/js/angularjs/1.6.10/angular.min.js'/>"></script>


<!--운동 자세히 보기 스크립트-->
<script>
	function showModal(exerciseName){
		$.ajax({
			url:"${pageContext.request.contextPath}/showExerciseService?exerciseName="+exerciseName,
					
					contentType: "application/json",
					
					success : function(json){
						 var jsonObj = JSON.parse(JSON.stringify(json));
						 var video = '<iframe width="600" height="315" src="'+jsonObj.exerciseUrl+'" frameborder="0" allowfullscreen></iframe>';;
						 
						 //var video="<video src='"+jsonObj.exerciseUrl+"' controls></video>";-원본 파일이 있을 때 쓰는 태그 방법
					 
						 console.log("video:"+video);
						$("#exerciseName").text(jsonObj.exerciseName); 
						$("#explain").text(jsonObj.explain);
						$("#exerciseUrl").html(video);
						 var imgF= '<img id="imgF" src="<c:url value="/image/'+ jsonObj.exerciseImgF + '" />"> '
						 var imgS= '<img id="imgS" src="<c:url value="/image/'+ jsonObj.exerciseImgS + '" />"> '
						 var imgT= '<img id="imgT" src="<c:url value="/image/'+ jsonObj.exerciseImgT + '" />"> '
						//'<img id="img'+(i+1)+ '" src="<c:url value="/image/'+ img + '" />" '
						$("#exerciseImgF").html(imgF);
						$("#exerciseImgS").html(imgS);
						$("#exerciseImgT").html(imgT); 						
						$("#exerciseModal").dialog();
						
					},
					error : function(xhr, status) {
		                alert(xhr+" : "+status);
		           }
			
		});//ajax
		
	}
	
</script>
<!--운동 자세히 보기 스크립트/-->

<!-- 운동 등록용 팝업창 띄우기 스크립트 시작-->
<script type="text/javascript">
	function writing() {
		window.open(
						"${pageContext.request.contextPath}/writing.do",
						"writingExercise",
						"width=400,height=400,toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes,left=50,right=50"
				)
	}

</script>
<!-- 운동 등록용 팝업창 띄우기 스크립트 /-->

<!-- 로그인 이벤트 스크립트 -->
<script language="javascript">
	$(document).ready(function() {
		
		 //메인 화면 초기 상태:로그 아웃 메뉴 은닉
		 $("#logout_menu").hide(); 
		 $("#login_menu").show();
		
		 //btnLogin(로그인 버튼)  
		 $('#btnLogin').click(function() {
			 //로그아웃 버튼 은닉.
			 $("#logout_menu").hide();
			 
			 $.ajax({
				 url : 'http://localhost:8383/Z_project/login/logincheck',
				 type : 'get',
				 dataType:'text',
				 data : {
					 id : $('#id').val(),
					 pw : $('#pw').val()
				 }, // data 
				 success : function(data) {
					 alert('데이터 전송 test:'+data);
					 if(data.trim()!=''){
						 
						 //로그인 성공 후, 메인 화면의 메시지 출력ex)진경용님이 로그인 헀습니다.						 
						 $("#afterLogin").html(data+'님이 로그인 했습니다.');
						 
			 			 //로그인 메뉴 은닉.로그아웃 메뉴 등장.
						 $("#logout_menu").show();
						 $("#login_menu").hide();
						 alert("로그인에 성공하셨습니다.");
						 
					 }else{
						 alert("로그인 실패");
					 }
					 
			 	} // success
			 }) // ajax
		 }); //btnLogin(로그인 버튼)/
		 		 
		 //logout_menu(로그 아웃)
		 $("#logout_menu").click(function(){
			 
			 //로그인 버튼 은닉
			 $("#login_menu").hide();
				
				//세션 종료
				 $("#logout_menu").attr("data-target","#myModal2");
				  
				 //시작
				 $.ajax({
					 url : 'http://localhost:8383/Z_project/login/logout',
					 type : 'get',
					 dataType:'text',
					 data : {
						 id : $('#id').val()
					 }, // data 
					 success : function(data) {
						 alert('로그아웃 데이터:'+data);
						 if(data.trim()=='true'){
							 alert('로그아웃 성공');
							 $("#afterLogin").html('좋은 하루되세요');

							 //로그인 메뉴 등장.로그아웃 메뉴 은닉
							 $("#login_menu").show();
							 $("#logout_menu").hide();

						 }

					 }
				 });
				 //끝
		 });
	});//
	//logout_menu(로그 아웃)
</script>
<!-- 로그인 이벤트 스크립트/ -->
</head>

<body>

<!-- 운동 상세설명 test시작 -->
<div id="exerciseModal" class="modal fade" role="dialog" title="글 상세보기">
	<div class="modal-dialog" >
		<div class="modal-content" style="height: 1100px; width: 800px;">
			<div>운동이름 :<section id="exerciseName"> </section></div>
			<div>설명:<section id="explain"></section></div>
			<div style="margin-left: 15%">동영상주소:<section id="exerciseUrl"></section></div>

				<!-- 캐러셀  -->
				<div class="container" style="width: 600px; height: 600px">
					<h2>Carousel Example</h2>
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->


						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">

							<div class="item active">
								<img alt="1번 이미지">
								<section id="exerciseImgF" style="width: 100%;"></section>
								<div class="carousel-caption"></div>
							</div>

							<div class="item">
								<img alt="2번 이미지" >
								<section id="exerciseImgS" style="width: 100%;"></section>
								<div class="carousel-caption"></div>
							</div>

							<div class="item">
								<img alt="3번 이미지">
								<section id="exerciseImgT" style="width: 100%;"></section>
								<div class="carousel-caption"></div>
							</div>

						</div>
						<a class="left carousel-control" href="#myCarousel"
							data-slide="prev"> <span
							class="glyphicon glyphicon-chevron-left"></span> <span
							class="sr-only">Previous</span>
						</a> <a class="right carousel-control" href="#myCarousel"
							data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right"></span> <span
							class="sr-only">Next</span>
						</a>

					</div>
				</div>
				<!-- 캐러셀 / -->
			</div>
	</div>
</div>
<!-- 운동 상세설명 test끝 -->

	
	<!-- 인자 전송 -->
	<div id="exercise_param" >
		<c:forEach items="${exerciseList}" var="exercise" varStatus="exer">
		 	<div id="exerciseName${exer.count}">${exercise.exerciseName}</div><!-- 운동이름-->&nbsp;	
			<div id="exerciseThumb${exer.count}">${exercise.thumbNail}</div><!-- 썸네일-->&nbsp;
			<br>
			<!-- count는 1부터 시작, index는 0부터 시작 -->
		</c:forEach>
	</div>
	<!-- 인자 전송 /-->

<!------------------------------------- 운영자 연락처 안내 시작 ------------------------------------->
	<div align="right">
		<!-- 로그인 세션정보 출력 -->
		message: ${msg}<br>
	       세션정보:${sessionScope.LOGIN_SESS}<br>
		
		<div id="afterLogin">좋은 하루 되세요.</div>
		
		<!-- 로그인 세션정보 출력/ -->
	
		<span>
			<img src="image/top/naver.jpg" width="25px" height="25px"><a href="#">네이버</a>
			<img src="image/top/facebook.jpg" width="25px" height="25px"><a href="#">페이스 북</a>
			<img src="image/top/kakao.jpg" width="25px" height="25px">
			<span style="text-decoration: blink">id=ruddydwls</span>&nbsp;&nbsp;
		</span>
	</div>
<!-------------------------------------- 운영자 연락처 안내 /-------------------------------------->


<!-------------------------------------navbar시작 -------------------------------------------->
	<div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
			
				<!-- 홈페이지 main제목-->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
					</button>
					<a class="navbar-brand" href="홈페이지 임시 디자인.html">home(추후 이미지 삽입)</a>
				</div>
				<!-- 홈페이지 main제목/-->

				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">HOME</a></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동 정보<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="insertExercise">운동정보</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동기구<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="운동기구 리스트창.html">운동기구</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">보충제<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="보충제 제품 리스트창.html">보충제 </a></li>
							</ul></li>
							
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">게시판<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="insertMemoBoard">게시판 </a></li>
							</ul></li>	
							

					</ul>
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" />
						</div>
						<button type="submit" class="btn btn-default">검색</button>

					</form>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="이벤트창.html"><span
								class="glyphicon glyphicon-list">이벤트</span></a></li>
						<li><a href="회원가입.html "><span
								class="glyphicon glyphicon-user">회원가입</span></a></li>
								
								<!-- 로그인 시작 -->
						<li><a id="login_menu" data-toggle="modal" data-target="#myModal2"><span
								class="glyphicon glyphicon-user" id="login_title">로그인</span></a></li>
								<!-- 로그인 // -->
								
								<!-- 로그 아웃 시작 -->
						<li><a id="logout_menu">
							<span class="glyphicon glyphicon-user" id="login_title">로그아웃</span></a>
						</li>	
								<!-- 로그 아웃// -->
									
						<li><a href="#"><span
								class="glyphicon glyphicon-shopping-cart">쇼핑카트</span></a></li>
						<li><a data-toggle="modal" href="#myModal"><span
								class="glyphicon glyphicon-user">내정보</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>


<!--------------------------------------navbar/ --------------------------------------------->


<!--------------------------------------Modal(내정보 확인)--------------------------------------->

	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<img src="image/photo.jpg" class="photo" width="130px"
						height="130px" style="float: left">
					<p>정보 1</p>
					<p>정보 2</p>
					<p>정보 3</p>

				</div>
				<div class="modal-body">
					<b>About Exercise</b>
					<div class="table-responsive">
						<table class="table">
							<thead style="background-color: #CCFF33">
								<tr>
									<th>운동 종류</th>
									<th>운동량</th>
									<th>운동 날짜</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>

							</tbody>
						</table>
					</div>
					<b>About Product</b>
					<div class="table-responsive">
						<table class="table">
							<thead style="background-color: #CCFF33">
								<tr>
									<th>제품 이름</th>
									<th>제품 가격</th>
									<th>제품 구매량</th>
									<th>총 금액</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
							</tbody>
						</table>
						<!-- 운동 및 제품 관련 내용 표시 끝-->
						<p>Hosted by: Jin Kyung</p>
						<p>
							Contact information: <a href="#"> ruddydwls@hanmail.com</a>.
						</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<!--------------------------------------Modal(내정보 확인)/--------------------------------------->



<!--------------------------------------Modal(로그인)-------------------------------------------->
	<div class="modal fade" id="myModal2" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">

					아이디<input type="text" id="id" name="id" >
					비밀번호<input type="text" id="pw" name="pw">
					 <input type="button" value="로그인" name="btnLogin" id="btnLogin" data-dismiss="modal">

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

<!--------------------------------------Modal(로그인)/--------------------------------------->


<!--------------------------------------운동 게시판--------------------------------------->
		
		<div id="wrapper">
			<!-- 테이블 -->
			<table border="1">
				<!-- 첫번쨰 행 -->
				<tr>
					<td id="1"></td>
					<td id="2"></td>
					<td id="3"></td>
				</tr>
				<!-- 두번쨰 행 -->
				<tr>
					<td id="4"></td>
					<td id="5"></td>
					<td id="6"></td>
				</tr>
			</table>
			<!-- 테이블/ -->
				
			<!-- 페이징  -->
			<div style="margin-left: 45%">
				 <a href="${pageContext.request.contextPath}/exerciseList/page/${prePage}/" class="btn btn-default">
     				 <span class="glyphicon glyphicon-chevron-left"></span> 이전
  				 </a>
				 <a href="${pageContext.request.contextPath}/exerciseList/page/${nextPage}/" class="btn btn-default">
      				<span class="glyphicon glyphicon-chevron-right"></span> 다음 
  				 </a>
  				 <a style="margin-left: 27%" class="btn btn-default" onclick="writing()">운동등록</a>
 				</div>
			<!-- 페이징 /-->
			
		</div>
		
	<!-- 운동 데이터 테이블 전송 스크립트 -->
	<script type="text/javascript">
		$(document).ready(function(){
			for(var i=0; i<6; i++){
				
				var txt=$("#exerciseName" +(i+1)).text();
				var img=$("#exerciseThumb" +(i+1)).text();
				var cell="#wrapper td#"+(i+1);
				 
			 	var exerciseContent 
	   	  	 	 = '<span id="exName'+(i+1)+ '">'+txt+'</span><br/>'+		
	   	  	 		'<img id="img'+(i+1)+ '" src="<c:url value="/image/'+ img + '" />" '+
	   	  	 		'data-toggle="modal" data-target="#exerciseModal" width="250" height="250">';  
	   	  	 		
				$("#wrapper").css("width", "100%");
				$("#wrapper").css("height", "500px");
				
				$("#wrapper table").attr("align","center");
				$(cell).html(exerciseContent);//인자값 세팅용
				
				$(cell).attr("width",'250px');//cell속성 가로 길이정의
				$(cell).attr("height",'250px');//cell속성 세로 길이정의
				
				$(cell).attr("align","center");//cell속성 가로 가운데 정렬
				$(cell).attr("valign","middle");//cell속성 세로 가운데 정렬
				
			}//for
		});//doc
		<!-- 운동 데이터 테이블 전송 스크립트 -->
		
		//운동 설명보기
		$(document).ready(function(){
			
			$("img[id^=img]").click(function(e){
				var id=e.target.id;
				var num = id.substring(3,4);
				var name =$("#exName"+num).text();
				showModal(name);
			});//click
		});//doc
		
	</script>
<!--------------------------------------운동 게시판--------------------------------------->
</body>
</html>
