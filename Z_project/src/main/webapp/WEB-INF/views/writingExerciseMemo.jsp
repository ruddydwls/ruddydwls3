<%
  /**
  * @Class Name : writingExerciseMemo.jsp
  * @Description : 각종 후기 게시판 작성 화면
  * @Modification Information
  *
  *   수정일               수정자                    수정내용
  *  -------    --------    ---------------------------
  *  2018.06.20 진경용                   에러 메시지(ng-model) 도입.
  *
  * author Jin Kyung Yong
  * since 2018.06.01
  *
  * Copyright (C) 2018 by JKY  All right reserved.
  */
%>
<%@ page contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko-kr">
<head>
<meta charset="utf-8" />
<title>게시판 글 쓰기</title>

<!-- angularjs 1.6.10 -->
<script	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.10/angular.min.js"></script>

<!-- jQuery : 3.3.1 -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<!--게시판 제목 대분류 설정 script  -->
<script>
 angular.module('boardJoinCheck', [])
 .controller('boardJoinCheckController', ['$scope', function($scope) {
    $scope.boxSelect = function() {
    if ($scope.boardKind == "") {
     alert("공백");//선택값이 "선택하세요"의 경우, 경고창 발생. 
     $scope.memoForm.boardKind.$valid = false;
     $scope.memoForm.box.submit_box.disabled = true;
    } //
    }; //

}]);
</script>
<!--게시판 제목 대분류 설정 script/ -->

<!-- 게시판 등록 날짜 스크립트 -->
<script type="text/javascript">
	$(document).ready(
			function() {
				alert($(window).height());
				var date = new Date();
				//10월 전에는 0이 붙도록 설정하고 그 후에는 그대로 출력하기
				var month = date.getMonth() + 1 < 10 ? '0'
						+ (date.getMonth() + 1) : date.getMonth() + 1;
				var today = "20" + date.getYear().toString().substring(1, 3)
						+ "-" + month + "-" + date.getDate() + " "
						+ date.getHours() + ":" + date.getMinutes() + ":"
						+ date.getSeconds();

				$("#boardDate").val(today);
			});//jquery
</script>
<!-- 게시판 등록 날짜 스크립트/ -->
</head>

<body ng-app="boardJoinCheck" ng-controller="boardJoinCheckController">

	<!-- 게시판 등록창 -->
	<div style="width: 1000px; height: 300px; margin: 0 auto;">

		<form:form commandName="memoForm" id="memoForm" name="memoForm"
			enctype="multipart/form-data" ng-model="memoForm"
			action="/Z_project/actionExerciseMemo" method="post">

			<!-- 사용자 id등록 -->
			<div>
				작성자 <input type="text" maxlength="20" id="id" name="id"
					ng-model="id" ng-minlength="5" ng-maxlength="20" />&nbsp;&nbsp;
				<!-- 작성자 필드 점검  -->
				<span id="id_msg_pnl">
					<!-- 메시지 출력부 -->
					<span id="id_msg" ng-model="id" ng-show="memoForm.id.$invalid">
					<font color="red">아이디는 8~20자로 작성해야합니다.</font>
					</span>
					<!-- 메시지 출력부/ -->
				</span>
				<!-- 작성자 필드 점검/ -->
			</div>
			<!-- 사용자 id등록/ -->


			<!-- 게시판 글 제목 -->
			<div>
				제목
				<!-- 게시판 글 대분류 설정-->
				<select id="box" name="boardKind" ng-model="box"
					ng-change="boxSelect()" required="true">
					<option value="">선택하세요</option>
					<option value="(운동)">운동</option>
					<option value="(제품)">제품</option>
				</select>
				<!-- 게시판 글 대분류 설정/-->
				<input type="text" maxlength="50"
					style="width: 500px; margin-left: 2%" id="boardTitle"
					name="boardTitle" ng-model="boardTitle" ng-minlength="1"
					ng-maxlength="50" />
				<!-- 게시판 글 폼 점검 -->
				<div id="boardTitle_msg_pnl" style="margin-left: 5%">
					<!-- 메시지 출력부 -->
					<div id="boardTitle_msg" ng-model="boardTitle"
						ng-show="memoForm.boardTitle.$invalid">
						<font color="red">제목은 1~50자로 작성해야합니다.</font>
					</div>
					<!-- 메시지 출력부/ -->
				</div>
				<!-- 게시판 글 폼 점검/ -->
			</div>
			<!--게시판 글 제목/-->


			<!-- 게시판 비밀번호 -->
			<div>
				<label>비밀번호<input type="text" maxlength="20" id="boardPass"
					name="boardPass" ng-model="boardPass" ng-minlength="8"
					ng-maxlength="20" /></label>
				<!-- 비밀번호 폼 점검 -->
				<span id="boardPass_msg_pnl">
					<!-- 메시지 출력부 --> 
					<span id="boardPass_msg" ng-model="boardPass"
						ng-show="memoForm.boardPass.$invalid">
						<font color="red">비밀번호는 8~20자로 작성해야합니다.</font>
					</span>
					<!-- 메시지 출력부 /-->	
				</span>
				<!-- 비밀번호 폼 점검/-->
			</div>
			<!-- 게시판 비밀번호/ -->


			<!-- 게시판 내용-->
			<div>내용</div>
			<textarea rows="10" cols="80" maxlength="1000" id="boardContent"
				name="boardContent" ng-model="boardContent" ng-minlength="10"
				ng-maxlength="1000"></textarea>

			<!-- 게시판 내용 폼 점검 -->
			<div id="boardContent_msg_pnl">
				<!-- 메시지 출력부 --> 
				<div id="boardContent_msg" ng-model="boardContent"
					ng-show="memoForm.boardContent.$invalid">
					<font color="red">1000자 이내로 입력해주시길 바랍니다.</font>
				</div>
				<!-- 메시지 출력부 /--> 
			</div>
			<!-- 게시판 내용 폼 점검/ -->
			<!-- 게시판 내용/-->

			<!-- 게시판 등록 날짜 -->
			<div>
				날짜<input type="text" maxlength="21" id="boardDate" name="boardDate"
					readonly="readonly" />
			</div>
			<!-- 게시판 등록 날짜/ -->
			

			<!-- 게시판 첨부파일 시작 -->
			<div>
				첨부파일<input type="file" maxlength="1000" id="boardFile"
					name="boardFile" />
			</div>
			<!-- 게시판 첨부파일 끝 -->
			

			<input type="submit" id="submit" name="submit" value="글쓰기"
				ng-model="submit_box">
			<!--  ng-disabled="memoForm.$invalid"> -->
		</form:form>

	</div>
	<!-- 게시판 등록창 /-->
</body>
</html>
