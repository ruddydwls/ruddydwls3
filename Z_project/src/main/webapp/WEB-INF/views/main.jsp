<%
  /**
  * @Class Name : main.jsp
  * @Description : 쇼핑몰 메인 화면
  * @Modification Information
  *
  *   수정일                 수정자                   수정내용
  *  -------     --------    ---------------------------
  *  2018.06.10  진경용                   이미지 경로 설정 변경(상대 경로->절대경로) 
  *
  * author Jin Kyung Yong
  * since 2018.06.01
  *
  * Copyright (C) 2018 by JKY  All right reserved.
  */
%>
<%@ page contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko-kr">
<head>
<meta charset="utf-8" />
<title>홈페이지 임시 디자인.</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>	
#wrapbox {
	direction: rtl
}
</style>


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!--jquery: 3.2.0  -->	
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<!--bootstrap:3.3.7  -->	
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--angularjs:1.6.9  -->	
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>

<!-- 로그인 이벤트 스크립트 -->
<script language="javascript">
	$(document).ready(function() {
		
		 //메인 화면 초기 상태:로그 아웃 메뉴 은닉
		 $("#logout_menu").hide(); 
		 $("#login_menu").show();
		
		 //btnLogin(로그인 버튼)  
		 $('#btnLogin').click(function() {
			 //로그아웃 버튼 은닉.
			 $("#logout_menu").hide();
			 
			 $.ajax({
				 url : 'http://localhost:8383/Z_project/login/logincheck',
				 type : 'get',
				 dataType:'text',
				 data : {
					 id : $('#id').val(),
					 pw : $('#pw').val()
				 }, // data 
				 success : function(data) {
					 alert('데이터 전송 test:'+data);
					 if(data.trim()!=''){
						 
						 //로그인 성공 후, 메인 화면의 메시지 출력ex)진경용님이 로그인 헀습니다.						 
						 $("#afterLogin").html(data+'님이 로그인 했습니다.');
						 
			 			 //로그인 메뉴 은닉.로그아웃 메뉴 등장.
						 $("#logout_menu").show();
						 $("#login_menu").hide();
						 alert("로그인에 성공하셨습니다.");
						 
					 }else{
						 alert("로그인 실패");
					 }
					 
			 	} // success
			 }) // ajax
		 }); //btnLogin(로그인 버튼)/
		 		 
		 //logout_menu(로그 아웃)
		 $("#logout_menu").click(function(){
			 
			 //로그인 버튼 은닉
			 $("#login_menu").hide();
				
				//세션 종료
				 $("#logout_menu").attr("data-target","#myModal2");
				  
				 //시작
				 $.ajax({
					 url : 'http://localhost:8383/Z_project/login/logout',
					 type : 'get',
					 dataType:'text',
					 data : {
						 id : $('#id').val()
					 }, // data 
					 success : function(data) {
						 alert('로그아웃 데이터:'+data);
						 if(data.trim()=='true'){
							 alert('로그아웃 성공');
							 $("#afterLogin").html('좋은 하루되세요');

							 //로그인 메뉴 등장.로그아웃 메뉴 은닉
							 $("#login_menu").show();
							 $("#logout_menu").hide();

						 }

					 }
				 });
				 //끝
		 });
	});//
	//logout_menu(로그 아웃)
</script>
<!-- 로그인 이벤트 스크립트/ -->

</head>

<body>

<!------------------------------------- 운영자 연락처 안내 시작 ------------------------------------->
	<div align="right">
		<!-- 로그인 세션정보 출력 -->
		message: ${msg}<br>
	       세션정보:${sessionScope.LOGIN_SESS}<br>
		
		<div id="afterLogin">좋은 하루 되세요.</div>
		
		<!-- 로그인 세션정보 출력/ -->
	
		<span>
			<img src="image/top/naver.jpg" width="25px" height="25px"><a href="#">네이버</a>
			<img src="image/top/facebook.jpg" width="25px" height="25px"><a href="#">페이스 북</a>
			<img src="image/top/kakao.jpg" width="25px" height="25px">
			<span style="text-decoration: blink">id=ruddydwls</span>&nbsp;&nbsp;
		</span>
	</div>
<!-------------------------------------- 운영자 연락처 안내 /-------------------------------------->


<!-------------------------------------navbar시작 -------------------------------------------->
	<div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
			
				<!-- 홈페이지 main제목-->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
					</button>
					<a class="navbar-brand" href="홈페이지 임시 디자인.html">home(추후 이미지 삽입)</a>
				</div>
				<!-- 홈페이지 main제목/-->

				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">HOME</a></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동 정보<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="insertExercise">운동정보</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동기구<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="운동기구 리스트창.html">운동기구</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">보충제<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="보충제 제품 리스트창.html">보충제 </a></li>
							</ul></li>
							
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">게시판<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="insertMemoBoard">게시판 </a></li>
							</ul></li>	
							

					</ul>
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" />
						</div>
						<button type="submit" class="btn btn-default">검색</button>

					</form>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="이벤트창.html"><span
								class="glyphicon glyphicon-list">이벤트</span></a></li>
						<li><a href="회원가입.html "><span
								class="glyphicon glyphicon-user">회원가입</span></a></li>
								
								<!-- 로그인 시작 -->
						<li><a id="login_menu" data-toggle="modal" data-target="#myModal2"><span
								class="glyphicon glyphicon-user" id="login_title">로그인</span></a></li>
								<!-- 로그인 // -->
								
								<!-- 로그 아웃 시작 -->
						<li><a id="logout_menu">
							<span class="glyphicon glyphicon-user" id="login_title">로그아웃</span></a>
						</li>	
								<!-- 로그 아웃// -->
									
						<li><a href="#"><span
								class="glyphicon glyphicon-shopping-cart">쇼핑카트</span></a></li>
						<li><a data-toggle="modal" href="#myModal"><span
								class="glyphicon glyphicon-user">내정보</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>


<!--------------------------------------navbar/ --------------------------------------------->


<!--------------------------------------Modal(내정보 확인)--------------------------------------->

	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<img src="image/photo.jpg" class="photo" width="130px"
						height="130px" style="float: left">
					<p>정보 1</p>
					<p>정보 2</p>
					<p>정보 3</p>

				</div>
				<div class="modal-body">
					<b>About Exercise</b>
					<div class="table-responsive">
						<table class="table">
							<thead style="background-color: #CCFF33">
								<tr>
									<th>운동 종류</th>
									<th>운동량</th>
									<th>운동 날짜</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>

							</tbody>
						</table>
					</div>
					<b>About Product</b>
					<div class="table-responsive">
						<table class="table">
							<thead style="background-color: #CCFF33">
								<tr>
									<th>제품 이름</th>
									<th>제품 가격</th>
									<th>제품 구매량</th>
									<th>총 금액</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
								<tr>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
									<td>추후 입력</td>
								</tr>
							</tbody>
						</table>
						<!-- 운동 및 제품 관련 내용 표시 끝-->
						<p>Hosted by: Jin Kyung</p>
						<p>
							Contact information: <a href="#"> ruddydwls@hanmail.com</a>.
						</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<!--------------------------------------Modal(내정보 확인)/--------------------------------------->



<!--------------------------------------Modal(로그인)-------------------------------------------->
	<div class="modal fade" id="myModal2" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">

					아이디<input type="text" id="id" name="id" >
					비밀번호<input type="text" id="pw" name="pw">
					 <input type="button" value="로그인" name="btnLogin" id="btnLogin" data-dismiss="modal">

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

<!--------------------------------------Modal(로그인)/--------------------------------------->



<!--------------------------------------메인 화면---------------------------------------------->

	<div>
		<div>
			<!--------------------제품 -------------------->
			<div class="container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#menu1">Best
							Seller</a></li>
					<li><a data-toggle="tab" href="#menu2">MD추천</a></li>
					<li><a data-toggle="tab" href="#menu3">New Arrival</a></li>
				</ul>

				<!-- Best Seller-->
				<div class="tab-content">

					<div id="menu1" class="tab-pane fade in active">
						<h3>Best Seller</h3>
						<div class="row">
							<!-- 1번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">

									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>

								</div>
							</div>
							<!-- 2번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
							<!-- 3번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<!-- 4번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">

									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">

										<p>연습용 이미지입니다.</p>
									</div>

								</div>
							</div>
							<!-- 5번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
							<!-- 6번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--best seller/-->


					<!-- MD추천 -->
					<div id="menu2" class="tab-pane fade">
						<h3>MD추천</h3>
						<div class="row">
							<!-- 1번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">

									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>

								</div>
							</div>
							<!-- 2번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
							<!-- 3번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>

						</div>
						<div class="row">
							<!-- 4번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">

									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>

								</div>
							</div>
							<!-- 5번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>

							<!-- 6번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- MD추천/-->


					<!-- New Arrival-->
					<div id="menu3" class="tab-pane fade">
						<h3>New Arrival</h3>
						<div class="row">
							<!-- 1번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>

							<!-- 2번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>

							<!-- 3번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<!-- 4번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">

									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
							<!-- 5번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
							<!-- 6번 이미지-->
							<div class="col-md-4">
								<div class="thumbnail">
									<img src="image/test.jpg" alt="Lights"
										style="width: 40%; height: 40%">
									<div class="caption">
										<p>연습용 이미지입니다.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- New Arrival/-->					
				</div>
			</div>
			<!--------------------제품/ -------------------->
			
			<!---------------- 회원 인스타 소개 ----------------->
			<div class="instagram" style="margin-right: 10%;">
				
				<div class="title" style="float: right">
					<span class="tit"> <a href="https://www.instagram.com/"
						target="_blank"> <img alt="인스타그램" src="image/insta_logo.jpg"
							style="width: 200px; height: 50px"></img></a>
					</span> <span class="tag"> <a href="https://www.instagram.com/"
						target="_blank">@kyungyongjin</a>
					</span>
					
					<div id="wrapbox">
						<!-- 인스타 이미지 div-->
						<span> <img src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span>
					</div>
					<div id="wrapbox">
						<!-- 인스타 이미지 div-->
						<span> <img src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span> <span> <img
							src="image/insta2.jpg" /></span> <span> <img
							src="image/insta1.jpg" /></span>
					</div>
				</div>
			</div>
			<!---------------- 회원 인스타 소개/ ----------------->
		</div>
	</div>
<!--------------------------------------메인 화면---------------------------------------------->


</body>

</html>
