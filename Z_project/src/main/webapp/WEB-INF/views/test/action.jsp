<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8"
				     pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>요청 인자 예제 : POST</title>
</head>
<body>
param1 : ${param1}<br> <!-- model.addAttribute -->
param2 : ${param2}<br> <!-- request.setAttribute -->
param3 : ${param3}<br> <!-- map -->
param4 : ${param4}<br>  <!-- ReqeustBody  : 한글 처리 잘 안됨 -->
param5 : ${param5}<br>  <!-- POJO : member -->
</body>
</html>
