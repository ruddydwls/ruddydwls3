
package com.jky.Z_project.DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jky.Z_project.VO.UserMapper;
import com.jky.Z_project.VO.UserVO;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SqlSession sqlsession;

	//INSERT UserVO
	@Override
	public String insertUser(UserVO user) {

		String msg = "";
		try {
			//UserMapper를 활용한 CLASS생성
			sqlsession.getMapper(UserMapper.class).insertUser(user);
			msg = "회원 정보를 성공적으로 삽입했습니다.";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "회원 정보 삽입에 실패 하였습니다";
		}

		return msg;
	}
	
	//DELETE UserVO
	@Override
	public String deleteUser(UserVO user) {

		String msg = "";
		try {
			//UserMapper를 활용한 CLASS생성
			sqlsession.getMapper(UserMapper.class).deleteUser(user);
			msg = "회원 정보를 성공적으로 삭제했습니다";
		} catch (Exception e) {
			msg = "회원 정보 삭제에 실패 하였습니다";
			e.printStackTrace();
		}

		return msg;
	}
	//UPDATE UserVO
	@Override
	public String updateUser(UserVO user) {
		String msg = "";
		try {
			//UserMapper를 활용한 CLASS생성
			sqlsession.getMapper(UserMapper.class).updateUser(user);
			msg = "회원 정보를 성공적으로 갱신했습니다.";
		} catch (Exception e) {
			msg = "회원 정보 갱신에 실패 하였습니다";
			e.printStackTrace();

		}

		return msg;
	}
	
	//GET UserVO
	@Override
	public UserVO getUser(String Id) {

		UserVO user = new UserVO();
		try {
			//UserMapper를 활용한 CLASS생성
			sqlsession.getMapper(UserMapper.class).getUser(Id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	
	//LIST UserVO
	@Override
	public List<UserVO> getAllUsers() {
		List<UserVO> list = new ArrayList<UserVO>();
		try {
			//UserMapper를 활용한 CLASS생성
			list = sqlsession.getMapper(UserMapper.class).getAllUsers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/** 
	 * @see com.jky.Z_project.DAO.UserDAO#isMember(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean isMember(String id, String pw) {
		boolean flag =false;
		try {
			//UserMapper를 활용한 CLASS생성
			flag=sqlsession.getMapper(UserMapper.class)
						   .isMember(new UserVO(id,pw))==1 ? true:false;
			
		} catch (Exception e) {
			e.printStackTrace();	
		}
		
		return flag;
	}
	
	
}
