package com.jky.Z_project.DAO;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jky.Z_project.VO.ExerciseRecordMapper;
import com.jky.Z_project.VO.ExerciseRecordVO;

import lombok.extern.java.Log;

@Repository
@Log
public class ExerciseRecordDAOImpl implements ExerciseRecordDAO {

	@Autowired
	private SqlSession sqlSession;
	
	//INSERT ExerciseRecord 
	@Override
	public String insertExerciseRecord(ExerciseRecordVO exerciseRecord) {
		String msg = "";
		try {
			//exerciseRecordMapper를 활용하여 클래스 생성
			sqlSession.getMapper(ExerciseRecordMapper.class).insertExerciseRecord(exerciseRecord);
			msg = "정상적으로 기록이 저장되었음(dao)";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "정상적으로 기록이 저장되었 않았음(dao)";
		}
		return msg;
	}
	
	// UPDATE ExerciseRecord
	@Override
	public String updateExerciseRecord(ExerciseRecordVO exerciseRecord) {
		String msg = "";
		try {
			
			//exerciseRecordMapper를 활용하여 클래스 생성
			sqlSession.getMapper(ExerciseRecordMapper.class).updateExerciseRecord(exerciseRecord);
			msg = "정상적으로 기록이 갱신되었음(dao)";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "정상적으로 기록이 갱신되지 않았음(dao)";
		}
		return msg;
	}
	
	//DELETE ExerciseRecord
	@Override
	public String deleteExerciseRecord(ExerciseRecordVO exerciseRecord) {
		String msg = "";
		try {
			//exerciseRecordMapper를 활용하여 클래스 생성
			sqlSession.getMapper(ExerciseRecordMapper.class).deleteExerciseRecord(exerciseRecord);
			msg = "정상적으로 기록이 삭제되었음(dao)";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "정상적으로 기록이 삭제되지 않았음(dao)";
		}
		return msg;
	}

	//GET ExerciseRecord
	@Override
	public ExerciseRecordVO getExerciseRecord(String exerciseRecord) {
		ExerciseRecordVO exerciserecord = new ExerciseRecordVO();

		try {
			//exerciseRecordMapper를 활용하여 클래스 생성
			exerciserecord = sqlSession.getMapper(ExerciseRecordMapper.class).getExerciseRecord(exerciseRecord);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return exerciserecord;
	}
	
	//LIST ExerciseRecord
	@Override
	public List<ExerciseRecordVO> getAllExerciseRecords() {
		log.info("getAllExerciseRecords()");

		try {
			//exerciseRecordMapper를 활용하여 클래스 생성
			return sqlSession.getMapper(ExerciseRecordMapper.class).getAllExerciseRecords();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
