package com.jky.Z_project.DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.jky.Z_project.VO.ProductMapper;
import com.jky.Z_project.VO.ProductVO;

@Repository
public class ProductDAOImpl implements ProductDAO {

	@Autowired
	private SqlSession sqlsession;
	
	//INSERT ProductVO
	@Override
	public String insertProduct(ProductVO product) {
		String msg = "";
		try {
			//productMapper를 활용하여 클래스 생성
			sqlsession.getMapper(ProductMapper.class).insertProduct(product);
			msg = "삽입 작업완료";
		} catch (Exception e) {
			msg = "삽입 작업 중 오류 발생";
			e.printStackTrace();
		}

		return msg;

	}
	
	//UPDATE ProductVO
	@Override
	public String updateProduct(ProductVO product) {
		String msg = "";
		try {
			//productMapper를 활용하여 클래스 생성
			sqlsession.getMapper(ProductMapper.class).updateProduct(product);
			msg = "갱신 작업 완료";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "갱신 작업 중 오류 발생.";
		}

		return msg;
	}
	//DELETE ProductVO
	@Override
	public String deleteProduct(ProductVO product) {
		String msg = "";
		try {
			//productMapper를 활용하여 클래스 생성
			sqlsession.getMapper(ProductMapper.class).deleteProduct(product);
			msg = "삭제 작업 완료";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "삭제 작업 중 오류 발생";
		}
		return msg;
	}
	//GET ProductVO
	@Override
	public ProductVO getProduct(String productId) {
		ProductVO product = new ProductVO();
		try {
			//productMapper를 활용하여 클래스 생성
			product = sqlsession.getMapper(ProductMapper.class).getProduct(productId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return product;
	}
	//LIST ProductVO
	@Override
	public List<ProductVO> getAllProducts() {
		List<ProductVO> list = new ArrayList<ProductVO>();
		try {
			//productMapper를 활용하여 클래스 생성
			list = sqlsession.getMapper(ProductMapper.class).getAllProducts();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
