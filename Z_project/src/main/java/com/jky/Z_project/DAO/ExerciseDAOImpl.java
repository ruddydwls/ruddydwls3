package com.jky.Z_project.DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jky.Z_project.VO.ExerciseMapper;
import com.jky.Z_project.VO.ExerciseVO;
import com.jky.Z_project.VO.PageParam;

import lombok.extern.java.Log;

@Repository
@Log
public class ExerciseDAOImpl implements ExerciseDAO {

	@Autowired
	private SqlSession sqlsession;

	//INSERT EXERCISE
	@Override
	public String insertExercise(ExerciseVO exercise) {
		String msg = "";
		try {
			
			//exerciseMapper를 활용하여 클래스 생성
			sqlsession.getMapper(ExerciseMapper.class).insertExercise(exercise);
			msg = "정상적으로 저장이 되었습니다.";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "정상적으로 저장이 되지 않았습니다.";
		}

		return msg;
	}

	//DELETE EXERCISE
	@Override
	public String deleteExercise(ExerciseVO exercise) {
		String msg = "";
		try {
			//exerciseMapper를 활용하여 클래스 생성
			sqlsession.getMapper(ExerciseMapper.class).deleteExercise(exercise);
			msg = "정상적으로 삭제가 되었습니다.";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "정상적으로 삭제가 되지 않았습니다.";
		}

		return msg;
	}
	
	//UPDATE EXERCISE
	@Override
	public String updateExercise(ExerciseVO exercise) {
		String msg = "";
		try {
			
			//exerciseMapper를 활용하여 클래스 생성
			sqlsession.getMapper(ExerciseMapper.class).updateExercise(exercise);
			msg = "정상적으로 갱신이 되었습니다.";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "정상적으로 갱신이 되지 않았습니다.";
		}

		return msg;
	}
	
	//GET EXERCISE
	@Override
	public ExerciseVO getExercise(String exerciseName) {
		ExerciseVO exercise = new ExerciseVO();
		try {
			//exerciseMapper를 활용하여 클래스 생성
			exercise = sqlsession.getMapper(ExerciseMapper.class).getExercise(exerciseName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return exercise;
	}
	
	//LIST EXERCISE
	@Override
	public List<ExerciseVO> getAllExercises() {
		List<ExerciseVO> list = new ArrayList<ExerciseVO>();
		try {
			//exerciseMapper를 활용하여 클래스 생성
			list = sqlsession.getMapper(ExerciseMapper.class).getAllExercises();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
	
	//PAGING EXERCISE
	@Override
	public List<ExerciseVO> getAllExercisesByPaging(PageParam pageParam) {
		log.info("getAllExercisesByPaging");
		try {
			//exerciseMapper를 활용하여 클래스 생성
			return sqlsession.getMapper(ExerciseMapper.class).getAllExercisesByPaging(pageParam);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
