package com.jky.Z_project.DAO;

import java.util.List;

import com.jky.Z_project.VO.ProductVO;
/**
 * @Class Name : ProductDAO.java
 * @Description : ProductDAO Class
 * @Modification Information
 * @
 * @  수정일                수정자                    수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.02  진경용                    최초생성
 *
 * @author 진경용
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
public interface ProductDAO {
	/**
	 * 제품을 추가한다.
	 * @param vo - 수정할 정보가 담긴 ProductVO
	 * @return 추가 결과
	 */
	String insertProduct(ProductVO product);
	
	/**
	 * 제품을 수정한다.
	 * @param vo - 수정할 정보가 담긴 ProductVO
	 * @return 수정결과
	 */
	String updateProduct(ProductVO product);
	
	/**
	 * 제품을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 ProductVO
	 * @return 삭제 결과
	 */
	String deleteProduct(ProductVO product);
	
	/**
	 * 제품 목록을 조회한다.
	 * @param ProductVO - 조회할 정보가 담긴 VO
	 * @return 제품 목록
	 */
	List<ProductVO> getAllProducts();
	
	/**
	 * 제품을 조회한다.
	 * @param vo - 조회할 정보가 담긴 ProductVO
	 * @return 조회한 제품
	 */
	ProductVO getProduct(String productId);
}
