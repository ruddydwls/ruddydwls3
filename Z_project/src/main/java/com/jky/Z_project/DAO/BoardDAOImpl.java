package com.jky.Z_project.DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jky.Z_project.VO.BoardMapper;
import com.jky.Z_project.VO.BoardVO;
import com.jky.Z_project.VO.PageParam;

import lombok.extern.java.Log;

@Repository
@Log
public class BoardDAOImpl implements BoardDAO {
	
	@Autowired
	private SqlSession sqlSession;
	
	//INSERT BOARD
	@Override
	public String insertBoard(BoardVO board) {
		String msg="";
		try {
			
			//BoardMapper를 사용해서 클래스 생성
			sqlSession.getMapper(BoardMapper.class).insertBoard(board);
			msg="정상적으로 저장이 됨";
		} catch (Exception e) {
			e.printStackTrace();
			msg="정상적으로 저장이 되지 않음(daoimpl문제)";
		}
		
		return msg;
	}
	
	//DELETE BOARD
	@Override
	public String deleteBoard(BoardVO board) {
		String msg="";
		try {
		
			//BoardMapper를 사용해서 클래스 생성
			sqlSession.getMapper(BoardMapper.class).deleteBoard(board);;
			msg="정상적으로 삭제 됨";
		} catch (Exception e) {
			e.printStackTrace();
			msg="정상적으로 삭제가 되지 않음(daoimpl문제)";
		}
		
		return msg;	}
	
	//UPDATE BOARD
	@Override
	public String updateBoard(BoardVO board) {
		String msg="";
		try {
			//BoardMapper를 사용해서 클래스 생성
			sqlSession.getMapper(BoardMapper.class).updateBoard(board);;
			msg="정상적으로 갱신이 됨";
		} catch (Exception e) {
			e.printStackTrace();
			msg="정상적으로 갱신이 되지 않음(daoimpl문제)";
		}
		
		return msg;
	}
	
	//GET BOARD
	@Override
	public BoardVO getBoard(int boardNum) {
		BoardVO board = new BoardVO();
		try {
			//BoardMapper를 사용해서 클래스 생성
			board = sqlSession.getMapper(BoardMapper.class).getBoard(boardNum);
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return board;
	}
	
	//LIST BOARD
	@Override
	public List<BoardVO> getAllBoards() {
		List<BoardVO> list = new ArrayList<BoardVO>();
		try {
			
			//BoardMapper를 사용해서 클래스 생성
			list = sqlSession.getMapper(BoardMapper.class).getAllBoards();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	//updateReadCount
	@Override
	public void updateReadCount(int boardNum) {
		try {
		
			//BoardMapper를 사용해서 클래스 생성
			sqlSession.getMapper(BoardMapper.class).updateReadCount(boardNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//PAGING BOARD
	@Override
	public List<BoardVO> getAllBoardsByPaging(PageParam pageParam) {
		log.info("getAllBoardsByPaging");
		try {
			
			//BoardMapper를 사용해서 클래스 생성
			return sqlSession.getMapper(BoardMapper.class).getAllBoardsByPaging(pageParam);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
