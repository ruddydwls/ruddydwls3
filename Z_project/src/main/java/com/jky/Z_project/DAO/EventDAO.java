package com.jky.Z_project.DAO;

import java.util.List;

import com.jky.Z_project.VO.EventVO;
/**
 * @Class Name : EventDAO.java
 * @Description : EventDAO Class
 * @Modification Information
 * @
 * @  수정일                수정자                    수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.02  진경용                    최초생성
 *
 * @author 진경용
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
public interface EventDAO {

	/**
	 * 이벤트를 등록한다.
	 * @param vo - 등록할 정보가 담긴 EventVO
	 * @return 등록결과
	 */
	String insertEvent(EventVO event);
	
	/**
	 * 이벤트를 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 EventVO
	 * @return 삭제 결과
	 */
	String deleteEvent(EventVO event);
	
	/**
	 * 이벤트를 수정한다.
	 * @param vo - 수정할 정보가 담긴 EventVO
	 * @return 갱신 결과
	 */
	String updateEvent(EventVO event);
	
	/**
	 * 이벤트를 조회한다.
	 * @param vo - 조회할 정보가 담긴 EventVO
	 * @return 조회한 이벤트
	 */
	EventVO getEvent(String EventName);
	
	/**
	 * 이벤트 목록을 조회한다.
	 * @param EventVO - 조회할 정보가 담긴 VO
	 * @return 이벤트 목록
	 */
	List<EventVO> getAllEvents();
	
	
}
