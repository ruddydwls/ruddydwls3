package com.jky.Z_project.DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jky.Z_project.VO.EventMapper;
import com.jky.Z_project.VO.EventVO;

@Repository
public class EventDAOImpl implements EventDAO {

	@Autowired
	private SqlSession sqlsession;

	//INSERT EVENT
	@Override
	public String insertEvent(EventVO event) {
		String msg="";
		try {
			//EventMapper를 활용해서 class를 생성
			sqlsession.getMapper(EventMapper.class).insertEvent(event);		
			msg= "정상적으로 저장됨";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "정상적으로 저장이 되지 않았음.";
		}
		return	msg; 
	}

	//DELETE EVENT
	@Override
	public String deleteEvent(EventVO event) {
		String msg="";
		try {
			//EventMapper를 활용해서 class를 생성
			sqlsession.getMapper(EventMapper.class).deleteEvent(event);		
			msg= "정상적으로 삭제됨";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "정상적으로 삭제가 되지 않았음.";
		}
		return	msg; 
	}
	
	//UPDATE EVENT	
	@Override
	public String updateEvent(EventVO event) {
		String msg ="";
		try {
			//EventMapper를 활용해서 class를 생성
			sqlsession.getMapper(EventMapper.class).updateEvent(event);
			msg ="정상적으로 갱신이 되었음";
		} catch (Exception e) {
			msg = "정상적으로 갱신이 되지 않았음";
		}
		return msg;
	}
	
	//GET EVENT
	@Override
	public EventVO getEvent(String EventName) {
		
		EventVO event = new EventVO();
		try {
			//EventMapper를 활용해서 class를 생성
			event = sqlsession.getMapper(EventMapper.class).getEvent(EventName);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return event;
	}

	//LIST EVENT
	@Override
	public List<EventVO> getAllEvents() {
		
		List<EventVO> list = new ArrayList<EventVO>();
		try {
			//EventMapper를 활용해서 class를 생성
			list = sqlsession.getMapper(EventMapper.class).getAllEvents();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
