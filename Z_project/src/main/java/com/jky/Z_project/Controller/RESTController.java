/**
 * 
 */
package com.jky.Z_project.Controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jky.Z_project.Service.UserService;

/**
 * @author a
 *
 */
@RestController
public class RESTController {

//*****************************************************************로그인 관련 restcontroller*****************************************************************
	@Autowired
	private UserService userService;
	
	
	@RequestMapping("/login/logincheck")
	public String loginCheck(@RequestParam("id") String id,@RequestParam("pw") String pw,
			HttpSession session) {

		System.out.println("id="+id);
		System.out.println("pw="+pw);
		
		boolean flag=userService.isMember(id, pw);
		if(flag==true && session.getAttribute("LOGIN_SESS")==null){   
		   session.setAttribute("LOGIN_SESS", id);
		
		}else {
			//비정상적인 로그인일 경우, null값이 아닌 공백으로 설정 후 jsp로 넘긴다.( 그 후 ajax에서 처리함)
			return "";
		}

		return session.getAttribute("LOGIN_SESS").toString();
	}
	
	@RequestMapping("/login/logout")
	public String logOut(/*@RequestParam("id") String id,*/ HttpSession session) {
		
		boolean flag = false;
		try {
			session.invalidate();
			flag= true;
		} catch (Exception e) {
			flag=false;
			e.printStackTrace();
		}
		return flag+"";
	}
//*****************************************************************로그인 관련 restcontroller 끝*************************************************************	
	
}
