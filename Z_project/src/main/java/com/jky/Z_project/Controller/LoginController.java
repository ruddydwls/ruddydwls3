package com.jky.Z_project.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jky.Z_project.VO.UserVO;

/**
 * 로그인을 위한 컨트롤러
 * @author a
 *
 */
//로그인 컨트롤러 
@Controller
@RequestMapping("/main")
public class LoginController {
	private static final Logger log = LoggerFactory.getLogger(LoginController.class);
	
	//로그인 컨트롤러
	@RequestMapping("/login")
	public String login(Model model) {
		log.info("login");
		model.addAttribute("UserVO", new UserVO());
		return "main";
	}
	//로그인 컨트롤러/

	//로그아웃 컨트롤러
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		log.info("logout");
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") != null) {
			session.invalidate();
			log.debug("로그아웃!!");
		}
		return "redirect:/main/login";

	}
	//로그아웃 컨트롤러/

	//로그인 인증 컨트롤러
	@RequestMapping(value = "/loginCheck", method = RequestMethod.POST)
	public String loginCheck(UserVO uservo, BindingResult bindingResult, HttpSession session, Model model) {
		log.info("loginCheck");
		String returnPath = "";
		String msg = "";
		System.out.println("uservo:"+ uservo);
		if (bindingResult.hasErrors()) {
			log.info("로그인 에러!");
			return "/main/login";
		} else {
			log.info("uservo:" + uservo);
			if (uservo.getId().equals("user_tbl.id") &&
					uservo.getPw().equals("user_tbl.pw")) {
				if (session.getAttribute("idSession") == null)
					session.setAttribute("idSession", uservo.getId());
				msg = "로그인에 성공하셨습니다";
			}else {
				msg="로그인에 실패했습니다";
			}
			model.addAttribute("msg", msg);
			returnPath = "main";
		}

		return returnPath;

	}
	//로그인 인증 컨트롤러/
}
