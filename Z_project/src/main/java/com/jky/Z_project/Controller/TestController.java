package com.jky.Z_project.Controller;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jky.Z_project.VO.BoardVO;

import lombok.extern.java.Log;

@Controller
@RequestMapping("/test")
@Log
public class TestController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "/test/test";
	}
		
	@RequestMapping(value = "/action", method = RequestMethod.POST)
	public String action(Model model,
									      HttpServletRequest request,
									      @RequestParam("id") String id,
									      @RequestParam Map<String, String>map,
									      @RequestBody String str,
									      BoardVO board) throws UnsupportedEncodingException {
		
		model.addAttribute("param1", id);
		request.setAttribute("param2", id);				
		model.addAttribute("param3", map.get("id"));
		
		model.addAttribute("param4", str.split("=")[1]);
		model.addAttribute("param5", board.getId());
		return "/test/action";
	} //
	
	@RequestMapping(value = "/actionExerciseMemo", 
				method = RequestMethod.POST)
	public String actionExerciseMemo(Model model,
					      HttpServletRequest request,
					      @RequestParam("id") String id,
					      @RequestParam Map<String, String>map,
					      @RequestBody String str,
					      BoardVO board) throws UnsupportedEncodingException {

		log.info("boardVO="+ board);
		model.addAttribute("param1", id);
		request.setAttribute("param2", id);				
		model.addAttribute("param3", map.get("id"));
		
		model.addAttribute("param4", str.split("=")[1]);
		model.addAttribute("param5", board.getId());
		return "/test/action";
	} //
}
