package com.jky.Z_project.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jky.Z_project.Service.BoardService;
import com.jky.Z_project.VO.BoardVO;
import com.jky.Z_project.VO.PageParam;
import com.jky.Z_project.VO.PageVO;

import lombok.extern.slf4j.Slf4j;
/**
 * 페이징 처리를 위한 컨트롤러
 * @author a
 *
 */
@Controller
@Slf4j
public class TestBbsController {
	
	@Autowired
	private BoardService svc;
	
	/**
	 * usage:http://localhost:8383/Z_project/testList/page/1/limit/5
	 * @param page
	 * @param limit
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/testList/page/{page}/limit/{limit}")
	public String testList(@PathVariable("page") int page,
							@PathVariable("limit") int limit,
						   Model model) {
		log.info("testList");
		page = page<=0 ? 1: page ;//페이지 설정(0이하면 1페이지로 적용)
		
		//총 게시글수 구하기
		int totalPageCount=svc.getAllBoards().size();
		
		//총 페이지 계산
		int maxPage =(int)((double)totalPageCount/limit+0.95);
		//현재 페이지에 보여줄 시작 페이지 수(1,11,21....)
		int startPage =(((int)((double)page/10+0.9))-1)*10+1;
		//현재 페이지에 보여줄 마지막 페이지 수(10,20,30....)
		int endPage = startPage+10-1;
		
		if(endPage>maxPage) endPage=maxPage;
		
		PageVO pageVo = new PageVO(); 
		
		pageVo.setEndPage(endPage);//
		pageVo.setListCount(totalPageCount);
		pageVo.setPage(page);
		pageVo.setStartPage(startPage);//
		pageVo.setMaxPage(maxPage);//
		
		model.addAttribute("pageInfo", pageVo);
		List<BoardVO> list =  svc.getAllBoardsByPaging(new PageParam(page, limit));
		model.addAttribute("boardList", svc.getAllBoardsByPaging(new PageParam(page, limit)));
		log.info("list.size"+list.size());
		
		return "exerciseMemo";
	}
	
	
}
