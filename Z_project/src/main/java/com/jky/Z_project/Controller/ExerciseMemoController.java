package com.jky.Z_project.Controller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jky.Z_project.Service.BoardService;
import com.jky.Z_project.VO.BoardDto;
import com.jky.Z_project.VO.BoardVO;
import com.jky.Z_project.VO.PageParam;
import com.jky.Z_project.VO.PageVO;

import lombok.extern.java.Log;

/**
 * 게시판 진입 및 신규 글 등록 그리고 게시판 페이징 처리를 위한 컨트롤러 
 * @author a
 *
 */
@Controller
@Log
public class ExerciseMemoController {
	

	@Autowired
    private FileSystemResource uploadDirResource; // 업로드 파일 처리

	@Autowired
	BoardService boardService;
	
	//운동 게시판 이동 및 페이징 처리 컨트롤러
	@RequestMapping("/insertMemoBoard")
	public ModelAndView enter(Model model) {
	
		//한페이지에 나올 글의 숫자를 제한.	
		int limit=5;
		int page=1;
		//총 게시글수 구하기
		int totalPageCount=boardService.getAllBoards().size();
		
		//페이지 계산
		int maxPage =(int)((double)totalPageCount/limit+0.95);
		//현재 페이지에 보여줄 시작 페이지 수(1,11,21....)
		int startPage =(((int)((double)page/10+0.9))-1)*10+1;
		//현재 페이지에 보여줄 마지막 페이지 수(10,20,30....)
		int endPage = startPage+10-1;
		
		if(endPage>maxPage) endPage=maxPage;
		
		PageVO pageVo = new PageVO(); 
		
		pageVo.setEndPage(endPage);
		pageVo.setListCount(totalPageCount);
		pageVo.setPage(page);
		pageVo.setStartPage(startPage);
		pageVo.setMaxPage(maxPage);
		
		ModelAndView model1=new ModelAndView();
		
		model1.addObject("boardList", boardService.getAllBoardsByPaging(new PageParam(page, limit)));
		
		model1.setViewName("exerciseMemo");
		model1.addObject("pageInfo", pageVo);
		
		return model1;
	}
	//운동 게시판 이동 및 페이징 처리 컨트롤러/

	//글쓰기 창 띄우기 컨트롤러
	@RequestMapping("/writingExerciseMemo")
	public String enter2(Model model) {
		log.info("/writingExerciseMemo");
		model.addAttribute("boardDto", new BoardDto());
		return "writingExerciseMemo";
	
	}
	//글쓰기 창 띄우기 컨트롤러/
	
	//게시판 글쓰기 컨트롤러
	@RequestMapping(value="/actionExerciseMemo",
				produces="text/html; charset=UTF-8",
				method=RequestMethod.POST)
	public ModelAndView write( @ModelAttribute("memoForm") BoardDto dto, RedirectAttributes ra) {
		System.out.println("/actionExerciseMemo");
		System.out.println(dto);
		BoardVO boardvo = new BoardVO(dto);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/success");
		
		
		MultipartFile file = dto.getBoardFile(); // 업로드 파일
		FileOutputStream fos = null;
		
		// 업로드 파일 처리
		if  (!dto.getBoardFile().isEmpty() && file != null) { // 파일 유효성 점검
			
			 String fileName = file.getOriginalFilename();
			  
            
             //  파일 저장소(F:\\uploadFile\\) 에 저장                             
             try {
	                  byte[] bytes = file.getBytes();
	        
	                  File outFileName
	                     = new File(uploadDirResource.getPath() + fileName);
	                  fos = new FileOutputStream(outFileName);
	                  
	                  fos.write(bytes);
                  
             } catch (IOException e) {
                 log.info("ExerciseMemoController save File writing error ! ");
                 e.printStackTrace();
             } finally {
                   try {    
                          if (fos !=null) fos.close();
                   } catch (IOException e) {
                           log.info("ExerciseMemoController save IOE : ");
                           e.printStackTrace();
                 }
                   log.info("File upload success! ");
             } // try
             
		} // 업로드 파일 처리
		
		//게시글 저장
		boardService.insertBoard(boardvo);
		return mv;
	}
	//게시판 글쓰기 컨트롤러/

	
	//게시판 글쓰기 성공 메시지 컨트롤러
	@RequestMapping("/success")
	public String success(Model model) {
		log.info("success:" );
		model.addAttribute("msg", "게시글 저장에 성공했습니다");
		
		return "/success/success";
	}
	//게시판 글쓰기 성공 메시지 컨트롤러/
	
		
	//각 페이지로 넘어가는 콘트롤러 
	@RequestMapping("/testList/boardNum/{boardNum}/page/{page}")
	public String boardDetail(@PathVariable("boardNum") int boardNum,
			@PathVariable("page") int page,
			Model model) {
		
		log.info("boardDetail");
		model.addAttribute("page", page);
		model.addAttribute("board", boardService.getBoard(boardNum));
		
		return "/boardView";
	}
	//각 페이지로 넘어가는 콘트롤러 끝
	
	//게시글 자세히 보기 컨트롤러
	@RequestMapping(value="showDetailService", produces="application/json; charset=UTF-8")
	@ResponseBody
	public String jsonFeedService(@RequestParam("boardNum") int boardNum ,Model model) {
			
		ObjectMapper mapper = new ObjectMapper();
		String json="";
		BoardVO boardVo = boardService.getBoard(boardNum);
		
		try {
			json= mapper.writeValueAsString(boardVo);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		log.info("페이지 자세히 보기:" + json);
		return json;
		
	}
	
	//게시글 자세히 보기 컨트롤러/	

}
