package com.jky.Z_project.Service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.jky.Z_project.VO.ProductMapper;
import com.jky.Z_project.VO.ProductVO;

public class ProductServiceImpl implements ProductService {

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private DataSourceTransactionManager transactionManager;

	//INSERT ProductVO
	@Override
	public String insertProduct(ProductVO product) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//productMapper를 활용하여 클래스 생성
		ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			productMapper.insertProduct(product);
			msg = "정상적으로 입력하였습니다";
		} catch (Exception e) {
			msg = "입력중 오류가 발생했습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}

	//UPDATE ProductVO
	@Override
	public String updateProduct(ProductVO product) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//productMapper를 활용하여 클래스 생성
		ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			productMapper.updateProduct(product);
			msg = "정상적으로 갱신이 되었습니다.";
		} catch (Exception e) {
			msg = "갱신중 문제가 발생하였습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;

	}
	
	//DELETE ProductVO
	@Override
	public String deleteProduct(ProductVO product) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//productMapper를 활용하여 클래스 생성
		ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);
		try {
			productMapper.deleteProduct(product);
			msg = "삭제가 성공적으로 완료되었습니다";
		} catch (Exception e) {
			msg = "삭제 중 문제가 발생하였습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}

	//GET ProductVO
	@Override
	public ProductVO getProduct(String productId) {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		ProductVO product = new ProductVO();
		
		//productMapper를 활용하여 클래스 생성
		ProductMapper productmapper = sqlSession.getMapper(ProductMapper.class);
		try {
			product = productmapper.getProduct(productId);
			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}
		return product;
	}

	//LIST ProductVO
	@Override
	public List<ProductVO> getAllProducts() {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		List<ProductVO> list = new ArrayList<ProductVO>();
		
		//productMapper를 활용하여 클래스 생성
		ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
		try {
			list = productMapper.getAllProducts();
			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}

		return list;
	}

}
