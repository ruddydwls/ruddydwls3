package com.jky.Z_project.Service;

import java.util.List;

import com.jky.Z_project.VO.ExerciseVO;
import com.jky.Z_project.VO.PageParam;
/**
 * @Class Name : ExerciseService.java
 * @Description : ExerciseService Class
 * @Modification Information
 * @
 * @  수정일                수정자                    수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.02  진경용                    최초생성
 *
 * @author 진경용
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
public interface ExerciseService {
	
	
	/**
	 * 운동을 등록한다.
	 * @param vo - 등록할 정보가 담긴 ExerciseVO
	 * @return 삽입결과
	 */
	String insertExercise(ExerciseVO exercise);
	
	
	/**
	 * 운동을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 ExerciseVO
	 * @return 삭제 결과
	 */
	String deleteExercise(ExerciseVO exercise);
	
	
	/**
	 * 운동을 수정한다.
	 * @param vo - 수정할 정보가 담긴 ExerciseVO
	 * @return 갱신결과
	 */
	String updateExercise(ExerciseVO exercise);
	
	/**
	 * 운동을 조회한다.
	 * @param vo - 조회할 정보가 담긴 ExerciseVO
	 * @return 조회한 글
	 */
	ExerciseVO getExercise(String exerciseName);
	
	
	/**
	 * 운동 목록을 조회한다.
	 * @param ExerciseVO - 조회할 정보가 담긴 VO
	 * @return 운동 목록
	 */
	List<ExerciseVO> getAllExercises();
	
	/**
	 * 페이징 처리를 한다.
	 * @param PageParam - 페이징 처리 정보를 담은 VO
	 * @return 페이지
	 */
	List<ExerciseVO> getAllExercisesByPaging(PageParam pageParam);
}
