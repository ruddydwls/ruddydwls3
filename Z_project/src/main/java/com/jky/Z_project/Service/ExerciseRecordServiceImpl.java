package com.jky.Z_project.Service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.jky.Z_project.VO.ExerciseRecordMapper;
import com.jky.Z_project.VO.ExerciseRecordVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExerciseRecordServiceImpl implements ExerciseRecordService {

	@Autowired
	private SqlSession sqlSession;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	//INSERT ExerciseRecord 
	@Transactional
	@Override
	public String insertExerciseRecord(ExerciseRecordVO exerciseRecord) {
		DefaultTransactionDefinition def= new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//exerciseRecordMapper를 활용하여 클래스 생성
		ExerciseRecordMapper exerciseRecordMapper  = sqlSession.getMapper(ExerciseRecordMapper.class);
		String msg ="";
		TransactionStatus status = transactionManager.getTransaction(def);
		try {
			exerciseRecordMapper.insertExerciseRecord(exerciseRecord);
			msg= "정상적으로 운동 기록을 저장하였습니다";
		} catch (Exception e) {
			msg="정상적으로 운동 기록을 저장되지 않았습니다";  
		}
		transactionManager.commit(status);
		return msg;
	}

	// UPDATE ExerciseRecord
	@Transactional
	@Override
	public String updateExerciseRecord(ExerciseRecordVO exerciseRecord) {
		DefaultTransactionDefinition def= new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//exerciseRecordMapper를 활용하여 클래스 생성
		ExerciseRecordMapper exerciseRecordMapper = sqlSession.getMapper(ExerciseRecordMapper.class );
		String msg ="";
		TransactionStatus status = transactionManager.getTransaction(def);
		
		try {
			msg = "성공적으로 기록을 갱신하였습니다";
			exerciseRecordMapper.updateExerciseRecord(exerciseRecord);
			
		} catch (Exception e) {
			msg ="성공적으로 기록을 갱신에 실패하였습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}
	
	//DELETE ExerciseRecord
	@Transactional
	@Override
	public String deleteExerciseRecord(ExerciseRecordVO exerciseRecord) {
		DefaultTransactionDefinition def= new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//exerciseRecordMapper를 활용하여 클래스 생성
		ExerciseRecordMapper exerciseRecordMapper = sqlSession.getMapper(ExerciseRecordMapper.class );
		String msg ="";
		TransactionStatus status = transactionManager.getTransaction(def);
			
		try {
			exerciseRecordMapper.deleteExerciseRecord(exerciseRecord);
			msg = "정상적으로 삭제가 되었습니다.";
		} catch (Exception e) {
			msg = "정상적으로 삭제가 되지 않았습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}
	
	//GET ExerciseRecord
	@Override
	public ExerciseRecordVO getExerciseRecord(String exerciseRecord) {

		TransactionDefinition def= new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		
		ExerciseRecordVO exerciserecord = new ExerciseRecordVO();
		
		//exerciseRecordMapper를 활용하여 클래스 생성
		ExerciseRecordMapper exerciserecordmapper = sqlSession.getMapper(ExerciseRecordMapper.class);
		 
		try {
			exerciserecord = exerciserecordmapper.getExerciseRecord(exerciseRecord);
			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}
		
		return exerciserecord;
	}
	
	//LIST ExerciseRecord
	@Override
	public List<ExerciseRecordVO> getAllExerciseRecords() {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		
		List<ExerciseRecordVO> list = new ArrayList<ExerciseRecordVO>();
		
		//exerciseRecordMapper를 활용하여 클래스 생성
		ExerciseRecordMapper exerciserecordmapper = sqlSession.getMapper(ExerciseRecordMapper.class);
		
		try {
			list = exerciserecordmapper.getAllExerciseRecords();
			transactionManager.commit(status);
			log.info("정상적으로 리스트를 출력하였습니다.");
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}
		
		return list;
	}

}
