package com.jky.Z_project.Service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.jky.Z_project.VO.ExerciseMapper;
import com.jky.Z_project.VO.ExerciseVO;
import com.jky.Z_project.VO.PageParam;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExerciseServiceImpl implements ExerciseService {

	@Autowired
	private SqlSession sqlsession;

	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	//INSERT EXERCISE
	@Transactional
	@Override
	public String insertExercise(ExerciseVO exercise) {

		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//exerciseMapper를 활용하여 클래스 생성
		ExerciseMapper exerciseMapper = sqlsession.getMapper(ExerciseMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);
		try {
			exerciseMapper.insertExercise(exercise);
			msg = "성공적으로 입력하였습니다.";
		} catch (Exception e) {
			msg = "입력에 실패하였습니다.";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}

	//DELETE EXERCISE
	@Transactional
	@Override
	public String deleteExercise(ExerciseVO exercise) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		//exerciseMapper를 활용하여 클래스 생성
		ExerciseMapper exerciseMapper = sqlsession.getMapper(ExerciseMapper.class);
		TransactionStatus status = transactionManager.getTransaction(def);
		String msg = "";
		try {
			exerciseMapper.deleteExercise(exercise);
			msg = "정상적으로 삭제하였습니다.";
		} catch (Exception e) {
			msg = "삭제에 실패했습니다.";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}

	//UPDATE EXERCISE
	@Transactional
	@Override
	public String updateExercise(ExerciseVO exercise) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//exerciseMapper를 활용하여 클래스 생성
		ExerciseMapper exerciseMapper = sqlsession.getMapper(ExerciseMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);
		try {
			exerciseMapper.updateExercise(exercise);
			msg = "성공적으로 갱신했습니다.";

		} catch (Exception e) {
			e.printStackTrace();
			msg = "갱신에 실패했습니다.";
		}
		transactionManager.commit(status);
		return msg;
	}

	//GET EXERCISE
	@Override
	public ExerciseVO getExercise(String exerciseName) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		ExerciseVO exercise = new ExerciseVO();
		
		//exerciseMapper를 활용하여 클래스 생성
		ExerciseMapper exerciseMapper = sqlsession.getMapper(ExerciseMapper.class);
		try {
			exercise = exerciseMapper.getExercise(exerciseName);
			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}

		return exercise;
	}

	//LIST EXERCISE
	@Override
	public List<ExerciseVO> getAllExercises() {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		List<ExerciseVO> list = new ArrayList<ExerciseVO>();
		
		//exerciseMapper를 활용하여 클래스 생성
		ExerciseMapper exerciseMapper = sqlsession.getMapper(ExerciseMapper.class);
		try {
			list = exerciseMapper.getAllExercises();
			transactionManager.commit(status);
			System.out.println("정상적으로 리스트를 출력중");
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}

		return list;
	}

	//PAGING EXERCISE
	@Transactional
	@Override
	public List<ExerciseVO> getAllExercisesByPaging(PageParam pageParam) {
		log.info("getAllExercisesByPaging");
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		List<ExerciseVO> exercises = null;
		ExerciseMapper exerciseMapper = sqlsession.getMapper(ExerciseMapper.class);

		try {
			exercises = exerciseMapper.getAllExercisesByPaging(pageParam);
			transactionManager.commit(status);
			log.info("정상적으로 게시글을 페이징 하였습니다.");
		} catch (Exception e) {
			log.info("정상적으로 게시글이 페이징 되지 않았습니다(service문제");
			e.printStackTrace();
			transactionManager.rollback(status);
		}

		return exercises;
	}

}
