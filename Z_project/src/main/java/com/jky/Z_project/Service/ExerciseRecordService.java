package com.jky.Z_project.Service;

import java.util.List;

import com.jky.Z_project.VO.ExerciseRecordVO;

/**
 * @Class Name : ExerciseRecordService.java
 * @Description : ExerciseRecordService Class
 * @Modification Information
 * @
 * @  수정일                수정자                    수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.02  진경용                    최초생성
 *
 * @author 진경용
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */


public interface ExerciseRecordService {
	
	/**
	 * 운동기록을 등록한다.
	 * @param vo - 등록할 정보가 담긴 ExerciseRecordVO
	 * @return 등록결과
	 */
	String insertExerciseRecord(ExerciseRecordVO exerciseRecord);
	
	/**
	 * 운동기록을 수정한다.
	 * @param vo - 수정할 정보가 담긴 ExerciseRecordVO
	 * @return 수정결과
	 */
	String updateExerciseRecord(ExerciseRecordVO exerciseRecord);
	
	/**
	 * 운동기록을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 ExerciseRecordVO
	 * @return 삭제 결과
	 */
	String deleteExerciseRecord(ExerciseRecordVO exerciseRecord);
	
	
	/**
	 * 운동기록을 조회한다.
	 * @param vo - 조회할 정보가 담긴 ExerciseRecordVO
	 * @return 조회한 운동기록
	 */
	ExerciseRecordVO getExerciseRecord(String exerciseRecord);
	
	/**
	 * 운동기록 목록을 조회한다.
	 * @param ExerciseRecordVO - 조회할 정보가 담긴 VO
	 * @return 운동기록 목록
	 */
	List<ExerciseRecordVO> getAllExerciseRecords();
}
