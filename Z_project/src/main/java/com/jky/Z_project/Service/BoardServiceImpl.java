package com.jky.Z_project.Service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.jky.Z_project.VO.BoardMapper;
import com.jky.Z_project.VO.BoardVO;
import com.jky.Z_project.VO.PageParam;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BoardServiceImpl implements BoardService {
	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private TransactionTemplate transactionTemplate;
	
	//INSERT BOARD			
	@Transactional
	@Override
	public String insertBoard(BoardVO board) {

		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//BoardMapper를 사용해서 클래스 생성
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			boardMapper.insertBoard(board);
			msg = "정상적으로 게시글이 저장되었습니다";
		} catch (Exception e) {
			msg = "정상적으로 게시글이 저장되지 않았습니다(service문제)";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}
	
	//DELETE BOARD
	@Transactional
	@Override
	public String deleteBoard(BoardVO board) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		//BoardMapper를 사용해서 클래스 생성
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			boardMapper.deleteBoard(board);
			msg = "정상적으로 게시글이 삭제되었습니다";
		} catch (Exception e) {
			msg = "정상적으로 게시글이 삭제되지 않았습니다(service문제)";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}
	
	//UPDATE BOARD
	@Transactional
	@Override
	public String updateBoard(BoardVO board) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//BoardMapper를 사용해서 클래스 생성
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			boardMapper.updateBoard(board);
			msg = "정상적으로 게시글이 갱신되었습니다";
		} catch (Exception e) {
			msg = "정상적으로 게시글이 갱신되지 않았습니다(service문제)";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}
	
	//GET BOARD
	@Override
	public BoardVO getBoard(int boardNum) {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		BoardVO board = new BoardVO();
		
		//BoardMapper를 사용해서 클래스 생성
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);

		try {
			board = boardMapper.getBoard(boardNum);
			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			System.out.println("serviceImpl문제입니다.");
			e.printStackTrace();
		}
		return board;
	}
	//LIST BOARD
	@Override
	public List<BoardVO> getAllBoards() {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		List<BoardVO> list = new ArrayList<BoardVO>();
		
		//BoardMapper를 사용해서 클래스 생성
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);

		try {
			list = boardMapper.getAllBoards();
			transactionManager.commit(status);
			System.out.println("정상적으로 리스트 출력됨");
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
			System.out.println("serviceImpl문제입니다.");
		}
		return list;

	}
	//updateReadCount
	@Transactional
	@Override
	public void updateReadCount(int boardNum) {
		System.out.println("updateReadCount");

		transactionTemplate.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				try {
					//BoardMapper를 사용해서 클래스 생성
					sqlSession.getMapper(BoardMapper.class).updateReadCount(boardNum);
				} catch (Exception e) {
					System.out.println("serviceImpl문제입니다.");
					status.setRollbackOnly();
				}

			}
		});
	}
	//PAGING BOARD
	@Transactional
	@Override
	public List<BoardVO> getAllBoardsByPaging(PageParam pageParam) {
		
		log.info("getAllBoardsByPaging");
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);
		List<BoardVO> boards = null;
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);

		try {
			boards= boardMapper.getAllBoardsByPaging(pageParam);
			transactionManager.commit(status);
			log.info("정상적으로 게시글이 페이징되었습니다");
			
		} catch (Exception e) {
			log.info("정상적으로 게시글이 페이징되지 않았습니다(service문제)");
			e.printStackTrace();
			transactionManager.rollback(status);
		}
		return boards;
	}

}
