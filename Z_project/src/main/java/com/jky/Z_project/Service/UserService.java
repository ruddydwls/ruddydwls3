package com.jky.Z_project.Service;

import java.util.List;

import com.jky.Z_project.VO.UserVO;
/**
 * @Class Name : UserService.java
 * @Description : UserService Class
 * @Modification Information
 * @
 * @  수정일                수정자                    수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.02  진경용                    최초생성
 *
 * @author 진경용
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
public interface UserService {
	
	/**
	 * 회원을 등록한다.
	 * @param vo - 등록할 정보가 담긴 UserVO
	 * @return 삽입 결과
	 */
	String insertUser(UserVO user);
	
	
	/**
	 * 회원을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 UserVO
	 * @return 삭제 결과
	 */
	String deleteUser(UserVO user);
	
	
	/**
	 * 회원을 수정한다.
	 * @param vo - 수정할 정보가 담긴 UserVO
	 * @return 갱신결과
	 */
	String updateUser(UserVO user);
	
	
	/**
	 * 회원 목록을 조회한다.
	 * @param UserVO - 조회할 정보가 담긴 VO
	 * @return 회원 목록
	 */
	List<UserVO> getAllUsers();
	
	/**
	 * 회원을 조회한다.
	 * @param vo - 조회할 정보가 담긴 UserVO
	 * @return 조회한 회원
	 */
	UserVO getUser(String Id);
	
	/**
	 * 회원여부인지를 조회한다.
	 * @param UserVO - 조회할 정보가 담긴 VO
	 * @return 회원 여부
	 */
	boolean isMember(String id, String pw);
}
