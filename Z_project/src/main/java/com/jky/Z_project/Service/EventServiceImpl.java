package com.jky.Z_project.Service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.jky.Z_project.VO.EventMapper;
import com.jky.Z_project.VO.EventVO;


@Service
public class EventServiceImpl implements EventService {

	@Autowired
	private SqlSession sqlsession;

	@Autowired
	private DataSourceTransactionManager transactionManager;

	//INSERT EVENT
	@Transactional
	@Override
	public String insertEvent(EventVO event) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		//EventMapper를 활용해서 class를 생성
		EventMapper eventMapper = sqlsession.getMapper(EventMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);
		try {
			eventMapper.insertEvent(event);
			msg = "정상적으로 저장이 되었습니다.";
		} catch (Exception e) {
			msg = "정상적으로 저장이 되지 않았습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}
	//DELETE EVENT
	@Transactional
	@Override
	public String deleteEvent(EventVO event) {

		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		//EventMapper를 활용해서 class를 생성
		EventMapper eventMapper = sqlsession.getMapper(EventMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);
		try {
			eventMapper.deleteEvent(event);
			msg = "정상적으로 삭제가 되었습니다.";
		} catch (Exception e) {
			msg = "정상적으로 삭제가 되지 않았습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}

	//UPDATE EVENT	
	@Transactional
	@Override
	public String updateEvent(EventVO event) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		//EventMapper를 활용해서 class를 생성
		EventMapper eventMapper = sqlsession.getMapper(EventMapper.class);
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			msg = "성공적으로 정보를 갱신했습니다.";
			eventMapper.updateEvent(event);
		} catch (Exception e) {
			msg = "정보 갱신에 실패했습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);

		return msg;
	}
	//GET EVENT
	@Override
	public EventVO getEvent(String EventName) {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		EventVO event = new EventVO();

		//EventMapper를 활용해서 class를 생성
		EventMapper eventmapper = sqlsession.getMapper(EventMapper.class);

		try {
			event = eventmapper.getEvent(EventName);
			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();

		}

		return event;
	}

	//LIST EVENT
	@Override
	public List<EventVO> getAllEvents() {

		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		List<EventVO> list = new ArrayList<EventVO>();

		//EventMapper를 활용해서 class를 생성
		EventMapper eventMapper = sqlsession.getMapper(EventMapper.class);

		try {
			list = eventMapper.getAllEvents();
			transactionManager.commit(status);
			System.out.println("정상적으로 리스트를 출력중");
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}

		return list;
	}
	

}
