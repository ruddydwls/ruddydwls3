package com.jky.Z_project.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.jky.Z_project.DAO.UserDAO;
import com.jky.Z_project.VO.UserVO;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDAO dao;

	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	//INSERT UserVO
	@Override
	public String insertUser(UserVO user) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			dao.insertUser(user);
			msg = "정상적으로 입력하였습니다";
		} catch (Exception e) {
			msg = "입력중 오류가 발생하였습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);

		return msg;
	}

	//DELETE UserVO
	@Override
	public String deleteUser(UserVO user) {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			dao.deleteUser(user);
			msg = "정상적으로 삭제 되었습니다.";
		} catch (Exception e) {
			msg = "삭제 중 문제가 발생하였습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}

	//UPDATE UserVO
	@Override
	public String updateUser(UserVO user) {

		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		String msg = "";
		TransactionStatus status = transactionManager.getTransaction(def);

		try {
			dao.updateUser(user);
			msg = "성공적으로 갱신하였습니다";
		} catch (Exception e) {
			msg = "갱신중 문제가 발생하였습니다";
			e.printStackTrace();
		}
		transactionManager.commit(status);
		return msg;
	}

	//GET UserVO
	@Override
	public UserVO getUser(String Id) {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		UserVO user = new UserVO();

		try {
			user= dao.getUser(Id);
			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}

		return user;
	}

	//LIST UserVO
	@Override
	public List<UserVO> getAllUsers() {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = transactionManager.getTransaction(def);

		List<UserVO> list = new ArrayList<UserVO>();

		try {
			list = dao.getAllUsers();
			transactionManager.commit(status);
		} catch (Exception e) {
			transactionManager.rollback(status);
			e.printStackTrace();
		}

		return list;
	}
	
	/** 
	 * @see com.jky.Z_project.Service.UserService#isMember(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean isMember(String id, String pw) {
		return dao.isMember(id, pw);
	}

}
