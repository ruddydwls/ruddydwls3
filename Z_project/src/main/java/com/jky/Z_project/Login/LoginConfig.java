package com.jky.Z_project.Login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class LoginConfig extends WebMvcConfigurerAdapter {
	private static final Logger log = LoggerFactory.getLogger(LoginConfig.class);
	@Autowired
	private LoginInterceptor loginInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		log.info("intercepter!!");
		registry.addInterceptor(loginInterceptor).addPathPatterns("/demo/**");

	}

}
