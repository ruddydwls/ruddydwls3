package com.jky.Z_project.Login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
/**
 * 인증여부 체크 인터셉터
 * @author 진경용
 * @since 2018.06.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일               수정자                   수정내용
 *  -------    --------    ---------------------------
 *  2011.06.23 진경용                    최초 생성
 *  </pre>
 */
@Component 
public class LoginInterceptor extends HandlerInterceptorAdapter {

	private static final Logger log = LoggerFactory.getLogger(LoginInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception{
		/**
		 * 세션에 로그인 세션정보가 있는지 여부로 인증 여부를 체크한다.
		 * 세션정보가 없다면, 메인 페이지로 이동한다.
		 */
		log.info("로그인 인터셉터:login interceptor");
		boolean loginFlag = false;
		
		if(request.getSession().getAttribute("idSession")!=null) {
			log.debug("로그인이 인증됨");
			loginFlag=true;
		}else {
			log.debug("로그인 미인증");
			response.sendRedirect(request.getContextPath()+"/main");
		}
		
				return loginFlag;
		
	} 
	@Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        
    }
 
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        
    }
	
	
}
