package com.jky.Z_project.VO;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

/**
 * @Class Name : BoardDto.java
 * @Description : BoardVO Class
 * @Modification Information
 * @
 * @  수정일                 수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.20               최초생성
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
@Data
public class BoardDto {
	// 맴버 필드 선언
	private int boardNum;// 게시판 글 번호(primary key)
	private String boardKind;//게시판 종류
	private String boardTitle;// 게시판 제목
	private String boardContent;// 글 내용
	private String boardPass;// 게시판 비밀번호
	private String boardDate;// 게시판 등록 날짜
	private MultipartFile boardFile;// 게시판에 첨부파일
	private int boardReadCount;// 게시판 글 조회수 count
	private String id;// foreign key(toward userVO)

	
}
