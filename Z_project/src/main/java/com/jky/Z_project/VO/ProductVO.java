package com.jky.Z_project.VO;

import lombok.Data;
/**
 * @Class Name : ProductVO.java
 * @Description : ProductVO Class
 * @Modification Information
 * @
 * @  수정일                 수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.03               최초생성
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
@Data
public class ProductVO {

	private String productId;//제품 아이디
	private String productName;//제품 이름
	private int productAmount;//제품 수량
	private int productPrice;//제품 가격

}
