package com.jky.Z_project.VO;

import java.util.List;
/**
 * 운동정보에 관한 데이터처리 매퍼 클래스
 *
 * @author  JIN KYUNG YONG
 * @since 2018.06.01
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *
 *  수정일                                       수정자                            수정내용
 *  ----------------    ------------    ---------------------------
 *   2016.06.08         진경용                             최초 생성
 *
 * </pre>
 */

public interface ProductMapper {
	/**
	 * 제품을 등록한다.
	 * @param vo - 등록할 정보가 담긴 ProductVO
	 * @return void형
	 */
	void insertProduct(ProductVO product);// 등록
	
	/**
	 * 제품을 수정한다.
	 * @param vo - 수정할 정보가 담긴 ProductVO
	 * @return void형
	 */
	void updateProduct(ProductVO product);// 정보 갱신
	
	/**
	 * 제품을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 ProductVO
	 * @return void형
	 */
	void deleteProduct(ProductVO product);// 정보 삭제

	/**
	 * 제품 목록을 조회한다.
	 * @param ProductVO - 조회할 정보가 담긴 VO
	 * @return 제품 목록
	 */
	List<ProductVO>getAllProducts();// list추출
	
	/**
	 * 제품을 조회한다.
	 * @param vo - 조회할 정보가 담긴 ProductVO
	 * @return 조회한 제품
	 */
	ProductVO getProduct(String productId);//productId를 기준으로 하여 특정  정보 추출.
}
