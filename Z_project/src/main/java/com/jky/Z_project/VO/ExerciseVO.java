package com.jky.Z_project.VO;

import lombok.Data;

/**
 * @Class Name : ExerciseVO.java
 * @Description : ExerciseVO Class
 * @Modification Information
 * @
 * @  수정일                  수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.07.02     진경용                     맴버필드 추가
 *                            (exerciseImgF,exerciseImgS,exerciseImgT,thumbNail추가)
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 2.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */


@Data
public class ExerciseVO {

	private String exerciseName;//운동이름
	private String exerciseImgF;//첫번째 사진
	private String exerciseImgS;//두번째 사진
	private String exerciseImgT;//세번째 사진(사진의 경우 view단에서 carousel 효과 부여)
	private String thumbNail;//썸네일
	private String explain;//운동 설명
	private String exerciseUrl;//동영상 주소(iFrame태그를 활용해서 유투브 영상을 활용함)



}