package com.jky.Z_project.VO;

import java.util.List;
/**
 * 운동에 관한 데이터처리 매퍼 클래스
 *
 * @author  JIN KYUNG YONG
 * @since 2018.06.01
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *
 *  수정일                                       수정자                            수정내용
 *  ----------------    ------------    ---------------------------
 *   2016.06.08         진경용                             최초 생성
 *
 * </pre>
 */


public interface ExerciseMapper {
	/**
	 * 운동을 등록한다.
	 * @param vo - 등록할 정보가 담긴 ExerciseVO
	 * @return void형
	 */
	void insertExercise(ExerciseVO exercise);
	
	/**
	 * 운동을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 ExerciseVO
	 * @return void형
	 */
	void updateExercise(ExerciseVO exercise); 
	
	
	
	/**
	 * 운동을 수정한다.
	 * @param vo - 수정할 정보가 담긴 ExerciseVO
	 * @return void형
	 */
	void deleteExercise(ExerciseVO exercise);

	/**
	 * 운동을 조회한다.
	 * @param vo - 조회할 정보가 담긴 ExerciseVO
	 * @return 조회한 글
	 */
	ExerciseVO getExercise(String exerciseName);
	
	/**
	 * 운동 목록을 조회한다.
	 * @param ExerciseVO - 조회할 정보가 담긴 VO
	 * @return 운동 목록
	 */
	List<ExerciseVO> getAllExercises();
	
	/**
	 * 페이징 처리를 한다.
	 * @param PageParam - 페이징 처리 정보를 담은 VO
	 * @return 페이지
	 */
	List<ExerciseVO> getAllExercisesByPaging(PageParam pageParam);	
	
}
