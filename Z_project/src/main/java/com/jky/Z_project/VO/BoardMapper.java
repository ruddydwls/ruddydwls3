package com.jky.Z_project.VO;

import java.util.List;
/**
 * 게시판글에 관한 데이터처리 매퍼 클래스
 *
 * @author  JIN KYUNG YONG
 * @since 2018.06.01
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *
 *  수정일                                       수정자                            수정내용
 *  ----------------    ------------    ---------------------------
 *   2016.06.08         진경용                             최초 생성
 *
 * </pre>
 */

public interface BoardMapper {

	/**
	 * 글을 등록한다.
	 * @param vo - 등록할 정보가 담긴 BoardVO
	 * @return void형
	 */
	void insertBoard(BoardVO board);
	
	
	/**
	 * 글을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 BoardVO
	 * @return void형
	 */
	void deleteBoard(BoardVO board);
	
	/**
	 * 글을 수정한다.
	 * @param vo - 수정할 정보가 담긴 BoardVO
	 * @return void형
	 */
	void updateBoard(BoardVO board);
	
	/**
	 * 글을 조회한다.
	 * @param vo - 조회할 정보가 담긴 BoardVO
	 * @return 조회한 글
	 */
	BoardVO getBoard(int boardNum);
	
	
	/**
	 * 글 목록을 조회한다.
	 * @param BoardVO - 조회할 정보가 담긴 VO
	 * @return 글 목록
	 */
	
	List<BoardVO> getAllBoards();
	
	/**
	 * 조회수를 등륵한다.
	 * @param boardNum - 조회수를 등록할 field
	 * @return 조회수
	 */
	void updateReadCount(int boardNum);
	
	/**
	 * 페이징 처리를 한다.
	 * @param PageParam - 페이징 처리 정보를 담은 VO
	 * @return 페이지
	 */
	List<BoardVO> getAllBoardsByPaging(PageParam pageParam);
		
}
