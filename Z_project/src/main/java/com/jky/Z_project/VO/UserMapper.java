package com.jky.Z_project.VO;

import java.util.List;

/**
 * 회원에 관한 데이터처리 매퍼 클래스
 *
 * @author  JIN KYUNG YONG
 * @since 2018.06.01
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *
 *  수정일                                       수정자                            수정내용
 *  ----------------    ------------    ---------------------------
 *   2016.06.08         진경용                             최초 생성
 *
 * </pre>
 */

public interface UserMapper {
	/**
	 * 회원을 등록한다.
	 * @param vo - 등록할 정보가 담긴 UserVO
	 * @return void형
	 */
	void insertUser(UserVO user);
	
	/**
	 * 회원을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 UserVO
	 * @return void형
	 */
	void deleteUser(UserVO user);
	
	/**
	 * 회원을 수정한다.
	 * @param vo - 수정할 정보가 담긴 UserVO
	 * @return void형
	 */
	void updateUser(UserVO user);
	
	/**
	 * 회원 목록을 조회한다.
	 * @param UserVO - 조회할 정보가 담긴 VO
	 * @return 회원 목록
	 */
	List<UserVO> getAllUsers();
	
	/**
	 * 회원을 조회한다.
	 * @param vo - 조회할 정보가 담긴 UserVO
	 * @return 조회한 회원
	 */
	UserVO getUser(String Id);
	
	/**
	 * 회원여부인지를 조회한다.
	 * @param UserVO - 조회할 정보가 담긴 VO
	 * @return 회원 여부
	 */
	
	int isMember(UserVO user);
	
}
