/**
 * 
 */
package com.jky.Z_project.VO;

import lombok.Data;

/**
 * @Class Name : ExerciseRecordVO.java
 * @Description : ExerciseRecordVO Class
 * @Modification Information
 * @
 * @  수정일                 수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.20               최초생성
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
@Data
public class ExerciseRecordVO {
	
	private String exerciseName;//운동 이름
	private String exerciseDate;//운동 날짜
	private int exerciseAmount;//운동량
		
}
