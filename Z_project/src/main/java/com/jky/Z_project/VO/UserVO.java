package com.jky.Z_project.VO;

import java.util.Date;

import lombok.Data;

/**
 * @Class Name : BoardDto.java
 * @Description : BoardVO Class
 * @Modification Information
 * @
 * @  수정일                 수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.20               최초생성
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */

@Data
public class UserVO {
	private String Id;//사용자 아이디
	private String Pw;//사용자 비밀번호
	private String Address;//사용자 주소
	private Date Joindate;//가입일
	private String Email;//이메일 주소
	
	/*
	 * 로그인시 활용될 메쏘드 선언
	 */
	public UserVO(String id, String pw) {
		Id = id;
		Pw = pw;
	}
	
	public UserVO() {
		
	}
	
}