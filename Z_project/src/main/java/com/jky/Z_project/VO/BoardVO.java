package com.jky.Z_project.VO;


import lombok.Data;

/**
 * @Class Name : BoardDto.java
 * @Description : BoardVO Class
 * @Modification Information
 * @
 * @  수정일                 수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.18  진경용                    lombok도입
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
@Data
public class BoardVO {
	// 맴버 필드 선언
	private int boardNum;// 게시판 글 번호(primary key)
	private String boardKind;//게시판 종류
	private String boardTitle;// 게시판 제목
	private String boardContent;// 글 내용
	private String boardPass;// 게시판 비밀번호
	private String boardDate;// 게시판 등록 날짜
	private String boardFile;// 게시판 첨부파일
	private int boardReadCount;// 게시판 글 조회수 count
	private String id;// foreign key(toward userVO)
	
	public BoardVO() {
		
	}
	/**
	 * @param boardNum:게시판 글 번호
	 * @param boardKind:게시판 종류
	 * @param boardTitle:게시판 제목
	 * @param boardContent:글 내용
	 * @param boardPass:게시판 비밀번호
	 * @param boardDate: 게시판 등록 날짜
	 * @param boardFile: 게시판 첨부 파일
	 * @param boardReadCount:게시판 글 조회수
	 * @param id:작성자
	 */
	public BoardVO(BoardDto dto) {
		this.boardNum = dto.getBoardNum();
		this.boardKind = dto.getBoardKind();
		this.boardTitle = dto.getBoardTitle();
		this.boardContent = dto.getBoardContent();
		this.boardPass = dto.getBoardPass();
		this.boardDate = dto.getBoardDate();
		this.boardFile = dto.getBoardFile().getOriginalFilename();
		this.boardReadCount = dto.getBoardReadCount();
		this.id = dto.getId();
	}

	
}
