package com.jky.Z_project.VO;

import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * @Class Name : PageParam.java
 * @Description : PageParam Class
 * @Modification Information
 * @
 * @  수정일                 수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.30   진경용                  최초생성
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */

@Data
@AllArgsConstructor
public class PageParam {
	
	private int page;//페이지
	private int limit;//1페이지에 인쇄될 게시물 수
		
	
}
