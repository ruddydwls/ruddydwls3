package com.jky.Z_project.VO;

import java.util.List;
/**
 * 이벤트글에 관한 데이터처리 매퍼 클래스
 *
 * @author  JIN KYUNG YONG
 * @since 2018.06.01
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *
 *  수정일                                       수정자                            수정내용
 *  ----------------    ------------    ---------------------------
 *   2016.06.08         진경용                            최초 생성
 *
 * </pre>
 */

public interface EventMapper {
	/**
	 * 이벤트를 등록한다.
	 * @param vo - 등록할 정보가 담긴 EventVO
	 * @return 등록결과
	 */
	void insertEvent(EventVO event);
	
	/**
	 * 이벤트를 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 EventVO
	 * @return 삭제 결과
	 */
	void deleteEvent(EventVO event);
	
	/**
	 * 이벤트를 수정한다.
	 * @param vo - 수정할 정보가 담긴 EventVO
	 * @return 수정결과
	 */
	void updateEvent(EventVO event);
	
	/**
	 * 이벤트를 조회한다.
	 * @param vo - 조회할 정보가 담긴 EventVO
	 * @return 조회한 이벤트
	 */
	EventVO getEvent(String Event_Name);
	
	/**
	 * 이벤트 목록을 조회한다.
	 * @param EventVO - 조회할 정보가 담긴 VO
	 * @return 이벤트 목록
	 */
	List<EventVO> getAllEvents();
}
