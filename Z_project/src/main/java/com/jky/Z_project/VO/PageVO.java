package com.jky.Z_project.VO;

import lombok.Data;

/**
 * @Class Name : PageVO.java
 * @Description : PageVO Class
 * @Modification Information
 * @
 * @  수정일                  수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.20  진경용                     최초생성
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */

@Data
public class PageVO {
	
	private int page;//현재 페이지 
	private int maxPage; //총 페이지(페이지 전체 숫자)
	private int startPage; //시작 페이지
	private int endPage;//마지막 페이지
	private int listCount;//페이지당 게시글수
}
