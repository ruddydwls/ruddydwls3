package com.jky.Z_project.VO;

import java.util.Date;

import lombok.Data;
/**
 * @Class Name : EventVO.java
 * @Description : EventVO Class
 * @Modification Information
 * @
 * @  수정일                 수정자                   수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2018.06.20               최초생성
 *
 * @author JIN KYUNG YONG
 * @since 2018. 06.01
 * @version 1.0
 * @see
 *
 *  Copyright (C) by JKY All right reserved.
 */
@Data
public class EventVO {
	
	private String eventCode;//이벤트 코드(PK)
	private String eventName;//이벤트 이름
	private Date eventDate;//이벤트 날짜

}
