<%@ page contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html lang="ko-kr">
<head>
<meta charset="utf-8" />
<title>게시판 글 쓰기</title>

<!-- angularjs 1.6.10 -->
<%-- <script src="<c:url value='/js/angularjs/1.6.10/angular.min.js'/>"></script> --%>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.10/angular.min.js"></script>
 

<!-- jQuery : 3.3.1 -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<%-- <script src="<c:url value='../webapp/views/js/jQuery/3.3.1/jquery-3.3.1.min.js'/>"></script> --%>
<%-- "<c:url value='/js/jQuery/3.3.1/jquery-3.3.1.min.js'/>" --%>
<!-- AngularJS -->
 <script>
 angular.module('boardJoinCheck', [])
 .controller('boardJoinCheckController', ['$scope', function($scope) {
    $scope.boxSelect = function() {
    if ($scope.boardKind == "") {
     alert("공백");//첫번쨰의 경우, 인자값을 넘기기 전, 경고 메시지 
     $scope.memoForm.boardKind.$valid = false;
     $scope.memoForm.box.submit_box.disabled = true;
    } //
    }; //

}]);
</script>

<!-- 금일 날짜 세팅 스크립트 -->
<!-- <script>
function outputDate() {
    
    var date = new Date();
    //10월 전에는 0이 붙도록 설정하고 그 후에는 그대로 출력하기
    var month = date.getMonth()+1 <10 ? '0'+ (date.getMonth()+1) : date.getMonth()+1;
    var today = "20"+date.getYear().toString().substring(1,3) + "-"
              + month + "-"
              + date.getDate() + " "
              + date.getHours() + ":"
              + date.getMinutes() + ":"
              + date.getSeconds();  
  			//  alert (today);
              document.all["temp"].innerText= today;
          
}

outputDate();    
</script> -->

<!-- 연습용 스크립트 -->
<script type="text/javascript">
	$(document).ready(function(){
		alert(1);
		alert($(window).height());
		var date = new Date();
	    //10월 전에는 0이 붙도록 설정하고 그 후에는 그대로 출력하기
	    var month = date.getMonth()+1 <10 ? '0'+ (date.getMonth()+1) : date.getMonth()+1;
	    var today = "20"+date.getYear().toString().substring(1,3) + "-"
	              + month + "-"
	              + date.getDate() + " "
	              + date.getHours() + ":"
	              + date.getMinutes() + ":"
	              + date.getSeconds();
                     
         //$("#temp").text(today);
         $("#boardDate").val(today);
	   	//			 document.getElementById("boardDate").innerText= today;
				
	});//jquery
	
</script>

</head>

<body ng-app="boardJoinCheck" 
	  ng-controller="boardJoinCheckController">
	  
<!-- 전체창 시작--->
<div style="width:1000px; height:300px; margin:0 auto;" >

  <form:form  commandName="memoForm" id="memoForm" name="memoForm" enctype="multipart/form-data"
  ng-model="memoForm" action="/Z_project/actionExerciseMemo" method="post">
  <!-- /Z_project/actionExerciseMemo -->
<%--   	<form id="memoForm" enctype="multipart/form-data" name="memoForm" ng-model="memoForm" action="/Z_project/test.do" method="post"> --%>
  	<!-- action.jsp:인자값 전송시 활용할 페이지  -->
<%-- <form  id="memoForm" name="memoForm" action="/Z_project/actionExerciseMemo"
 	method="post" enctype="multipart/form-data" > --%>
<!-- enctype="multipart/form-data" -->

<!-- 사용자 시작 -->
	<div>
	작성자 <input type="text" maxlength="20" id="id" name="id" 
						   ng-model="id"
	                       ng-minlength="5"
	                       ng-maxlength="20"	                       
	                       />&nbsp;&nbsp;
	<!-- 폼점검 메시지 시작 -->
		<span id="id_msg_pnl"> 
		    <!-- 아이디 필드 점검 -->
		    <span id="id_msg" 
		         ng-model="id"
		    	  ng-show="memoForm.id.$invalid">
		 	<font color="red">아이디는 8~20자로 작성해야합니다.</font>
		 	</span>
		</span>
	<!-- 폼점검 메시지 끝 -->
	</div>
<!-- 사용자 끝 -->


<!-- 글제목 시작-->
<div>
제목
<!-- 게시판 글 대분류 설정  시작-->
<select id="box" name="boardKind" ng-model="box" ng-change="boxSelect()" required="true">
    <option value="">선택하세요</option>
    <option value="(운동)">운동</option>
    <option value="(제품)">제품</option>
  </select>

<!-- 게시판 글 대분류 설정 끝-->

<input type="text" maxlength="50" style="width:500px; margin-left: 2%" id="boardTitle" name="boardTitle"
					   ng-model="boardTitle"
                       ng-minlength="1"
                       ng-maxlength="50"
                        />  
                                              
                       <!-- 글제목 폼 점검 -->
<div id="boardTitle_msg_pnl" style="margin-left: 5%"> 
    <!-- 아이디 필드 점검 -->
    <div id="boardTitle_msg" 
         ng-model="boardTitle"
    	  ng-show="memoForm.boardTitle.$invalid">
 		<font color="red">제목은 1~50자로 작성해야합니다.</font>
 	</div>
 </div>           
</div>
<!--글제목 끝-->


<!-- 비밀번호 입력란 시작-->
<div>
<label>비밀번호<input type="text" maxlength="20" id="boardPass" name="boardPass"
					ng-model="boardPass" ng-minlength="8" ng-maxlength="20"	/></label>

	<!-- 비밀번호 폼 점검 -->
	<span id="boardPass_msg_pnl"> 
	    <!-- 비밀번호 필드 점검 -->
	    <span id="boardPass_msg" 
	         ng-model="boardPass"
	    	  ng-show="memoForm.boardPass.$invalid">
	 		<font color="red">비밀번호는 8~20자로 작성해야합니다.</font>
	 	</span>
	 </span>
	<!-- 비밀번호 폼 점검 끝-->
</div>
<!-- 비밀번호 입력란 끝-->


<!-- 내용란 시작-->
<div>
내용
</div>
<textarea rows="10" cols="80" maxlength="1000" id="boardContent" name="boardContent"
		ng-model="boardContent" ng-minlength="10" ng-maxlength="1000"></textarea>

<!-- 글 내용 폼 점검 -->
<div id="boardContent_msg_pnl"> 
    <!-- 글 내용 필드 점검 -->
    <div id="boardContent_msg" 
         ng-model="boardContent"
    	  ng-show="memoForm.boardContent.$invalid">
 	<font color="red">1000자 이내로 입력해주시길 바랍니다.</font>
 	</div>
 </div>
<!-- 내용란 끝-->

<div>
날짜<input type="text" maxlength="21" id="boardDate" name="boardDate" readonly="readonly"/>
</div>

<!--  첨부파일 시작 -->
<div>
첨부파일<input type="file" maxlength="1000" id="boardFile" name="boardFile"/>
</div>
<!--  첨부파일 끝 -->

<input type="submit" id="submit" name="submit" value="글쓰기" 
	ng-model="submit_box"><!--  ng-disabled="memoForm.$invalid"> -->
</form:form>

</div>
<!-- 전체장 끝--->
</body>
</html>
