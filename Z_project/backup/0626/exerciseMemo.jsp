<%@ page contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<HTML lang="ko-kr">
<head>
<meta charset="utf-8" />
<title>후기창.</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>무제 문서</title>
<!--select 용 스타일-->
<style>
select {
	width: 100px;
	height: 31px;
	padding-left: 10px;
	font-size: 18px;
	color: #006fff;
	border-radius: 3px;
}
</style>

<!-- 페이징용 스타일-->
<style>
.pagination {
	display: inline-block;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
	border: 1px solid #ddd;
}

.pagination a.active {
	background-color: #4CAF50;
	color: white;
	border: 1px solid #4CAF50;
}

.pagination
 
a
:hover
:not
 
(
.active
 
)
{
background-color
:
 
#ddd
;

}
.pagination a:first-child {
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
}

.pagination a:last-child {
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
}
</style>

<style>
.logo {
	width: 500px;
	margin-left: 45%
}
</style>


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script language="javascript">
	function showPopup() {
		window
				.open(
						"https://ssl.pstatic.net/sstatic/keypage/outside/timer/timer_160627_2.html",
						"a", "width=600, height=300, left=100, top=50");
	}
	
	function openWriting(){
		window.open("${pageContext.request.contextPath}/writingExerciseMemo","writingExerciseMemo"
				,"width=1000,height=350,toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes,left=50,right=50");
		}
</script>    
    
    
</head>


<body>

<div>
<!-- 게시판 인자 -->
총게시글 수:${pageInfo.listCount}<br/>
현재 페이지 :${pageInfo.page}<br/>
총 생성되는 페이지 :${pageInfo.maxPage}<br/>
시작 페이지:${pageInfo.startPage}<br/>
끝 페이지:${pageInfo.endPage}<br/>
</div>

<br/>
<div>
	<c:forEach items="${boardList}" var="board" varStatus="st">
		${st.count}, ${board.boardNum},${board.boardTitle},${board.boardDate},${board.boardReadCount}<br/>
	</c:forEach>
</div> 

	<div align="right">
		<span><img src="<c:url value='/image/top/naver.jpg' />" /><a href="#">네이버</a> 
			<img src="홈페이지 제작/image/top/facebook.jpg" width="25px"  height="25px"><a
			href="#">페이스 북</a> 
			<img src="홈페이지 제작/image/top/kakao.jpg" width="25px"
			 height="25px"> <span style="text-decoration: blink">id=ruddydwls</span>
			&nbsp;&nbsp;</span>
	</div>

	<!--navbar시작 -->
	<div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
					</button>
					<a class="navbar-brand" href="홈페이지 제작/홈페이지 임시 디자인.html">home(추후 이미지 삽입)</a>
				</div>

				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">HOME</a></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동 정보<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="홈페이지 제작/운동 게시판.html">운동정보</a></li>
								<li><a href="insertMemoBoard">내 운동자랑하기</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동기구<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="홈페이지 제작/운동기구 리스트창.html">운동기구</a></li>
								<li><a href="홈페이지 제작/운동기구후기창.html">후기</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">보충제<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="홈페이지 제작/보충제 제품 리스트창.html">보충제 </a></li>
								<li><a href="홈페이지 제작/보충제후기창.html">후기</a></li>
							</ul></li>

					</ul>
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" />
						</div>
						<button type="submit" class="btn btn-default">검색</button>

					</form>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="#" onclick="showPopup();"><span
								class="glyphicon glyphicon-time">타이머</span></a></li>
						<li><a href="홈페이지 제작/회원가입.html "><span
								class="glyphicon glyphicon-user">회원가입</span></a></li>
						<li><a href="#"><span class="glyphicon glyphicon-log-in">로그인</span></a></li>
						<li><a href="홈페이지 제작/이벤트창.html"><span
								class="glyphicon glyphicon-list">이벤트</span></a></li>
						<li><a href="#"><span
								class="glyphicon glyphicon-shopping-cart">쇼핑카트</span></a></li>
						<li><a data-toggle="modal" href="#myModal"><span
								class="glyphicon glyphicon-user">내정보</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div class="logo">운동 후기창 게시판입니다.</div>

	<!-- 테이블 시작-->
	<div class="container">
		<div>
			<!-- 내 글 확인하기 버튼-->
			<span style="float: right"><input type="button"
				value="내 글 확인하기" /> </span>
		</div>
		<!-- 게시판 글 부분 시작 -->
		<c:if test="${not empty boardList and pageInfo.listCount>0}">
		<table class="table table-condensed">
			<thead>
				<tr>
					<th style="width: 10%">NO</th>
					<th style="width: 50%">제목</th>
					<th style="width: 10%">사용자</th>
					<th style="width: 20%">DATE</th>
					<th style="width: 10%">조회</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${boardList}" var="board" varStatus="st">
					<tr>
						<td>${board.boardNum}</td>
						
						<!-- 수정한 부분 시작 -->
						<td><a href="${pageContext.request.contextPath}/testList/boardNum/${board.boardNum}/page/${pageInfo.page}/">
						${board.boardKind} ${board.boardTitle}</a></td>
						<!-- 수정한 부분 끝 -->
						
						<td>${board.id}</td>
						<td>${board.boardDate}</td>
						<td>${board.boardReadCount}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
<!-- 게시판 글 부분 끝 -->        


<!-- 게시판 인자 -->
<%-- 총게시글 수:${pageInfo.listCount}<br/>
현재 페이지 :${pageInfo.page}<br/>
총 생성되는 페이지 :${pageInfo.maxPage}<br/>
시작 페이지:${pageInfo.startPage}<br/>
끝 페이지:${pageInfo.endPage}<br/>
</div> --%>


<!--  페이징 시작 -->
	<section>
		<ul class="pagination">
		
		<!-- 처음으로 paging -->
			<c:choose>
				<c:when test="${pageInfo.page<=1}">
				<!-- 주의) 이 부분에서 bootstrap 페이징 적용시 불가피하게 <a> 기입. <a>없으면 적용 안됨. -->
					<li> <a href="${pageContext.request.contextPath}/testList/page/1/limit/5"> 처음으로</a></li>
				</c:when>
				
				<c:otherwise>
					<li> <a href="${pageContext.request.contextPath}/testList/page/1/limit/5"> 처음으로</a></li>
				</c:otherwise>
			</c:choose>
		<!-- 이전으로 paging -->
			<c:choose>
				<c:when test="${pageInfo.page<=1}">
				<!-- 주의) 이 부분에서 bootstrap 페이징 적용시 불가피하게 <a> 기입. <a>없으면 적용 안됨. -->
					<li> <a href="${pageContext.request.contextPath}/testList/page/1/limit/5"> 이전</a></li>
				</c:when>
				
				<c:otherwise>
					<li> <a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page-1}/limit/5"> 이전</a></li>
				</c:otherwise>
			</c:choose>
			
			<c:forEach var="a" begin="${pageInfo.startPage}" end="${pageInfo.startPage}">
				<c:choose>
					<c:when test="${a == pageInfo.page}">
						<li class="active"><a href="${pageContext.request.contextPath}/testList/${pageInfo.page}">${pageInfo.page}</a></li>
						<li><a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+1}/limit/5">${pageInfo.page+1}</a></li>
						<li><a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+2}/limit/5">${pageInfo.page+2}</a></li>
					</c:when>
					<c:otherwise>
						<li class="active"><a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page}/limit/5">${pageInfo.page}</a></li>
						<li><a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+1}/limit/5">${pageInfo.page+1}</a></li>
						<li><a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+2}/limit/5">${pageInfo.page+2}</a></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		<!-- 다음으로 paging -->
			
			<c:choose>
				<c:when test="">
					<li> <a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+1}/limit/5"> 다음</a></li>
				</c:when>
	
				<c:otherwise>
					<li> <a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page+1}/limit/5"> 다음</a></li>
				</c:otherwise>
			</c:choose>
	
		</ul>
	
	</section>
</c:if>

		<!-- 등록글 없을 경우 메시지 띄우기. -->
<c:if test="${empty boardList || pageInfo.listCount==0}">
	<c:choose>
		<c:when test="${empty boardList || pageInfo.listCount==0}">
		<!-- 주의) 이 부분에서 bootstrap 페이징 적용시 불가피하게 <a> 기입. <a>없으면 적용 안됨. -->
			<section id="emptyArea">등록된 글이 없습니다.</section>
			 <a href="${pageContext.request.contextPath}/testList/page/${pageInfo.page-1}/limit/5"> 
				 <button type="button" class="btn btn-info">이전</button></a>
		</c:when>
	</c:choose>
</c:if>

		<!-- 페이징 끝 -->       
 		<!-- 페이징 시작-->
<!-- 		<div class="pagination">
			<a href="#">&laquo;</a> <a href="#">1</a> <a href="#">2</a> <a
				href="#">3</a> <a href="#">4</a> <a href="#">5</a> <a href="#">6</a>
			<a href="#">&raquo;</a>
		</div> -->
		
		
		
		
		<!--버튼 및 검색 창 구현하기-->
		<div style="float: right">
			<form class="navbar-form navbar-left" action="/action_page.php">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search" /> <select>
						<option value="검색">검색</option>
						<option value="제목">제목</option>
						<option value="내용">내용</option>
						<option value="작성자">작성자</option>
					</select>
					<button type="submit" class="btn btn-default">검색</button>
 					<input type="button" class="btn btn-default" onclick="openWriting()" value="글쓰기"/>
                    
                   </div>
			</form>
		</div>
        
	</div>
	<!-- 후기 테이블 끝-->
    
</body>
</html>
