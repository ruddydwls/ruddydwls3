<%@ page contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	${list.get(0)}
	<br />
	<table border="1" cellspacing="0" cellpadding="10">
		<tr>
			<td>아이디</td>
			<td>이벤트 이름</td>
			<td>이벤트 날짜</td>
		</tr>

		<c:forEach var="e" items="${list}">
			<tr>
				<td>${e.id}</td>
				<td>${e.eventName}</td>
				<td>${e.eventDate}</td>
			</tr>
		</c:forEach>
	</table>
	<input type="button" value="입력" />
	<input type="button" value="삭제" />

</body>
</html>