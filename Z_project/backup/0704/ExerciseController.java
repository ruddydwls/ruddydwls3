package com.jky.Z_project.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jky.Z_project.Service.ExerciseService;
import com.jky.Z_project.VO.ExerciseVO;
import com.jky.Z_project.VO.PageParam;
import com.jky.Z_project.VO.PageVO;

import lombok.extern.java.Log;

@Controller
@Log
public class ExerciseController {
	
	@Autowired
	ExerciseService exerciseService;

	//main->exercise.jsp로 진입 콘트럴러 시작(6개 단위로 뿌려주기)
	@RequestMapping("/insertExercise")
	public ModelAndView exercise(Model model) {
		log.info("exercise 페이지 진입");
		
		int limit =6;//페이지당 나올 게시글 수
		int page =1;//시작 페이지 NO
		int totalPageCount = exerciseService.getAllExercises().size();//전체 페이지 수 설정
		
		int prePage=1;
		int nextPage=1;
		
		int maxPage =(int)((double)totalPageCount/limit+0.95);
		//현재 페이지에 보여줄 시작 페이지 수(1,11,21....)
		int startPage =(((int)((double)page/10+0.9))-1)*10+1;
		//현재 페이지에 보여줄 마지막 페이지 수(10,20,30....)
		int endPage = startPage+10-1;
		
		PageVO pageVo = new PageVO();

		pageVo.setEndPage(endPage);//
		pageVo.setListCount(totalPageCount);
		pageVo.setPage(page);
		pageVo.setStartPage(startPage);//
		pageVo.setMaxPage(maxPage);//
		
		prePage= page==1 ? 1:page-1;
		nextPage= page==1 ? 2:page+1;

		ModelAndView model1 = new ModelAndView();
		
		model1.addObject("exerciseList", exerciseService.getAllExercisesByPaging(new PageParam(page, limit)));
		
		model1.setViewName("exerciseList");
		model1.addObject("pageinfo", pageVo);
		model.addAttribute("prePage", prePage);		
		model.addAttribute("nextPage", nextPage);
		model.addAttribute("totalPageCount", totalPageCount);
		model.addAttribute("maxPage", maxPage);
	
		return model1;
				
	}
	//main->exercise.jsp로 진입 콘트럴러 끝
	
	//페이징  컨트롤러 시작
	@RequestMapping(value="/exerciseList/page/{page}", produces="application/json; charset=UTF-8")
	//@ResponseBody
	public String exerciseDetail(@PathVariable("page") int page, Model model) {
		log.info("exerciseDetail");
		log.info("page:"+page);
		int prePage=1;
		int nextPage=1;
		prePage= page==1 ? 1:page-1;
		nextPage= page==1 ? 2:page+1;
		List<ExerciseVO> list =exerciseService.getAllExercisesByPaging(new PageParam(page, 6));
		model.addAttribute("page", page);
//		model.addAttribute("exercise", exerciseService.getAllExercises());
		model.addAttribute("exerciseList", list);

		model.addAttribute("prePage", prePage);		
		model.addAttribute("nextPage", nextPage);
		
		return "/exerciseList";
		//return list.toString();
	}
	//페이징  컨트롤러 끝
	
	//각 cell별 자세한 내용 보기
	@RequestMapping(value="showExerciseService", produces="application/json; charset=UTF-8")
	@ResponseBody
	public String jsonFeedService(@RequestParam("exerciseName") String exerciseName,Model model ) {
		ObjectMapper mapper= new ObjectMapper();
		String json="";
		ExerciseVO exerciseVo = exerciseService.getExercise(exerciseName);
		try {
			json = mapper.writeValueAsString(exerciseVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
			log.info("페이지 자세히 보기:" + json);
			return json;
		
		
	}

	
}
