<%@ page contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="ko-kr"> 
<head>
<meta charset="utf-8" />
<title>홈페이지 임시 디자인.</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>
#wrapbox {
	direction: rtl;
}

#exercise_param{
height:0;
background-color: cyan;
overflow: hidden;
}
</style>


<!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /> -->
<link rel="stylesheet"
	href="<c:url value='/js/bootstrap/3.3.7/css/bootstrap.min.css'/>" />

<!-- <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script> -->
	
<script
	src="<c:url value='/js/jQuery/3.3.1/jquery-3.3.1.min.js'/>"></script>	
<!-- <script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
	
<script
	src="<c:url value='/js/bootstrap/3.3.7/js/bootstrap.min.js' />"></script>	
<script
	src="<c:url value='/js/angularjs/1.6.10/angular.min.js'/>"></script>


<!-- 신규  스크립트 :운동 자세히 보기-->
<script>

	function showDetail(exerciseName){
		alert("인자 불어오는 테스트1");
		$.ajax({
			url:"${pageContext.request.contextPath}/showExerciseService?exerciseName="+exerciseName,
					
					contentType: "application/json",
					
					success : function(json){
						alert("인자 불어오는 테스트2");
						var jsonObj = JSON.parse(JSON.stringify(json));
						
						$("#exerciseName").text(jsonObj.exerciseName); 
						$("#exerciseImgF").text(jsonObj.exerciseImgF);
						$("#exerciseImgS").text(jsonObj.exerciseImgS);
						$("#exerciseImgT").text(jsonObj.exerciseImgT); 
						$("#explain").text(jsonObj.explain);
						$("#exerciseUrl").text(jsonObj.exerciseUrl);
						$("#exerciseModal").dialog();
						
					},
					error : function(xhr, status) {
		                alert(xhr+" : "+status);
		           }
			
		});//ajax
		
	}
	
	//테스트용 함수 2번
	
	function showModal(exerciseName){
		$.ajax({
			url:"${pageContext.request.contextPath}/showExerciseService?exerciseName="+exerciseName,
					
					contentType: "application/json",
					
					success : function(json){
						 var jsonObj = JSON.parse(JSON.stringify(json));
						$("#exerciseName").text(jsonObj.exerciseName); 
						$("#explain").text(jsonObj.explain);
						$("#exerciseUrl").text(jsonObj.exerciseUrl); 
						$("#exerciseModal").dialog();
						
						//exerciseModal
						
					},
					error : function(xhr, status) {
		                alert(xhr+" : "+status);
		           }
			
		});//ajax
		
	}
	
	
	
</script>

</head>

<body>

<!-- 운동 상세설명 test시작 -->
<div id="exerciseModal" class="modal fade" role="dialog" title="글 상세보기">
	<div class="modal-dialog">
		<div class="modal-content">
			운동이름 :<section id="exerciseName"> </section>
			설명:<section id="explain"></section>
			동영상주소:<section id="exerciseUrl"> </section>
		</div>
	</div>
</div>
<!-- 운동 상세설명 test끝 -->


<div>
<!-- 게시판 인자 -->
현재 페이지 :${page} <br/>
운동 이름:${exerciseList.get(0).exerciseName}<br/>
</div>


<a href="${pageContext.request.contextPath}/exerciseList/cell/1/page/1/" 
data-toggle="modal" data-target="#boardView">
<button type="button" class="btn btn-primary btn-xs" name="showDetail" 
data-toggle="modal" data-target="#exerciseModal"
id="showDetail">상세보기</button></a>

<!-- 지울곳 마무리 -->

	<!-- count는 1부터 시작, index는 0부터 시작 -->
	<!-- 인자 전송 test용 시작 -->
	<div id="exercise_param" >
		<c:forEach items="${exerciseList}" var="exercise" varStatus="exer">
	 
 	<div id="exerciseName${exer.count}">${exercise.exerciseName}</div><!-- 운동이름-->&nbsp;	
	<div id="exerciseThumb${exer.count}">${exercise.thumbNail}</div><!-- 썸네일-->&nbsp;
<%-- 	<div id="exerciseImgF${exer.count}">${exercise.exerciseImgF}</div><!-- 첫번쨰 사진-->&nbsp;
	<div id="exerciseImgS${exer.count}">${exercise.exerciseImgS}</div><!-- 첫번쨰 사진-->&nbsp;
	<div id="exerciseImgT${exer.count}">${exercise.exerciseImgT}</div><!-- 첫번쨰 사진-->&nbsp;
	<div id="exerciseExplain${exer.count}">${exercise.exerciseExplain}</div><!-- 운동 설명-->&nbsp;
	<div id="exerciseUrl${exer.count}">${exercise.exerciseUrl}</div><!-- 동영상주소-->&nbsp; --%>
			<br>
		</c:forEach>

	</div>
	<!-- 인자 전송 test용 끝-->

	<!-- 운영자 연락처 소개란.-->
	<div align="right">
		<span><img src="image/top/naver.jpg" width="25" height="25"><a
			href="#">네이버</a> <img src="image/top/facebook.jpg" width="25"
			height="25"><a href="#">페이스 북</a> <img
			src="image/top/kakao.jpg" width="25" height="25"> <span
			style="text-decoration: blink">id=ruddydwls</span> &nbsp;&nbsp;</span>
	</div>

	<!--navbar시작 -->
	<div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
					</button>
					<a class="navbar-brand" href="홈페이지 임시 디자인.html">home(추후 이미지 삽입)</a>
				</div>

				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">HOME</a></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동 정보<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="운동 게시판.html">운동정보</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동기구<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="운동기구 리스트창.html">운동기구</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">보충제<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="보충제 제품 리스트창.html">보충제 </a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">게시판<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="insertMemoBoard">게시판 </a></li>
							</ul></li>

					</ul>
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" />
						</div>
						<button type="submit" class="btn btn-default">검색</button>

					</form>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="#"><span class="glyphicon glyphicon-log-in">로그인</span></a></li>
						<li><a href="이벤트창.html"><span
								class="glyphicon glyphicon-list">이벤트</span></a></li>
						<li><a href="회원가입.html "><span
								class="glyphicon glyphicon-user">회원가입</span></a></li>
						<li><a href="#"><span
								class="glyphicon glyphicon-shopping-cart">쇼핑카트</span></a></li>
						<li><a data-toggle="modal" href="#myModal"><span
								class="glyphicon glyphicon-user">내정보</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>


	<!--*****************************************************************nav-bar끝 *****************************************************************-->


	<!--********************************************************** 운동 화면 작업 시작 *****************************************************************-->
		
		<!-- 데이터 뿌려주는 곳 시작 -->
		<div id="wrapper">
			<table border="1">
				<!-- 첫번쨰 행 -->
				<tr>
					<td id="1"></td>
					<td id="2"></td>
					<td id="3"></td>
				</tr>
				<!-- 두번쨰 행 -->
				<tr>
					<td id="4"></td>
					<td id="5"></td>
					<td id="6"></td>
				</tr>
				</table>
				
				<!-- 페이징 시작 -->
			 <a href="${pageContext.request.contextPath}/exerciseList/page/${prePage}/"> 이전</a>
			 <a href="${pageContext.request.contextPath}/exerciseList/page/${nextPage}/"> 다음</a>
				<!-- 페이징 끝 -->
				
		</div>
		<!-- 데이터 뿌려주는 곳 끝 -->
		
	<!-- 테이블 설정 스크립트 시작 -->
	<script type="text/javascript">
		$(document).ready(function(){
			for(var i=0; i<6; i++){
				
				var txt=$("#exerciseName" +(i+1)).text();
				var img=$("#exerciseThumb" +(i+1)).text();
				var cell="#wrapper td#"+(i+1);
				 
			 	var exerciseContent 
	   	  	 	 = '<span id="exName'+(i+1)+ '">'+txt+'</span><br/>'+		
	   	  	 		'<img id="img'+(i+1)+ '" src="<c:url value="/image/'+ img + '" />" '+
	   	  	 		'data-toggle="modal" data-target="#exerciseModal" width="250" height="250">';  
	   	  	 		
	   	  	 		
	   	  	 		
   	  	 	/* <a href="${pageContext.request.contextPath}/exerciseList/cell/${cell}/page/${page}/" 
   	  	 	data-toggle="modal" data-target="#exerciseView"> */	
	   	  	 		
				$("#wrapper").css("width", "100%");
				$("#wrapper").css("height", "500px");
				
				$("#wrapper table").attr("align","center");
				$(cell).html(exerciseContent);//인자값 세팅용
				
				$(cell).attr("width",'250px');//cell속성 가로 길이정의
				$(cell).attr("height",'250px');//cell속성 세로 길이정의
				
				$(cell).attr("align","center");//cell속성 가로 가운데 정렬
				$(cell).attr("valign","middle");//cell속성 세로 가운데 정렬
				
			}//for
		});//doc
		
		//운동 설명보기
		$(document).ready(function(){
			
			$("img[id^=img]").click(function(e){
				var id=e.target.id;
				var num = id.substring(3,4);
				var name =$("#exName"+num).text();
				showModal(name);
			});//click
		});//doc끝
		
	</script>
<!-- 테이블 설정 스크립트 끝 -->

</body>

</html>
