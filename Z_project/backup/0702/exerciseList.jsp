<%@ page contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<head>
<meta charset="utf-8" />
<title>홈페이지 임시 디자인.</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<style>
#wrapbox {
	direction: rtl
}
</style>


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>

<script language="javascript">
	function showPopup() {
		window
				.open(
						"https://ssl.pstatic.net/sstatic/keypage/outside/timer/timer_160627_2.html",
						"a", "width=400, height=300, left=100, top=50");
	}
</script>

</head>

<body>
	<!-- 인자 전송 test용 시작 -->
	<div>
		<c:forEach items="${exerciseList}" var="exercise" varStatus="exer">
	 
 	${exercise.exerciseName}<!-- 운동이름-->&nbsp;	
	${exercise.exerciseImgF}<!-- 첫번쨰 사진-->&nbsp;
	${exercise.exerciseImgS}<!-- 두번쨰 사진-->&nbsp;
	${exercise.exerciseImgT}<!-- 세번쨰 사진-->&nbsp;
	${exercise.thumbNail}<!-- 썸네일-->&nbsp;
	${exercise.explain}<!-- 운동 설명-->&nbsp;
	${exercise.exerciseUrl}<!-- 동영상 주소-->
			<br>
		</c:forEach>

	</div>
	<!-- 인자 전송 test용 끝-->

	<!-- 운영자 연락처 소개란.-->
	<div align="right">
		<span><img src="image/top/naver.jpg" width="25" height="25"><a
			href="#">네이버</a> <img src="image/top/facebook.jpg" width="25"
			height="25"><a href="#">페이스 북</a> <img
			src="image/top/kakao.jpg" width="25" height="25"> <span
			style="text-decoration: blink">id=ruddydwls</span> &nbsp;&nbsp;</span>
	</div>

	<!--navbar시작 -->
	<div>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
						<div class="icon-bar"></div>
					</button>
					<a class="navbar-brand" href="홈페이지 임시 디자인.html">home(추후 이미지 삽입)</a>
				</div>

				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">HOME</a></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동 정보<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="운동 게시판.html">운동정보</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">운동기구<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="운동기구 리스트창.html">운동기구</a></li>
							</ul></li>

						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">보충제<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="보충제 제품 리스트창.html">보충제 </a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">게시판<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="insertMemoBoard">게시판 </a></li>
							</ul></li>

					</ul>
					<form class="navbar-form navbar-left" action="/action_page.php">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" />
						</div>
						<button type="submit" class="btn btn-default">검색</button>

					</form>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="#"><span class="glyphicon glyphicon-log-in">로그인</span></a></li>
						<li><a href="이벤트창.html"><span
								class="glyphicon glyphicon-list">이벤트</span></a></li>
						<li><a href="회원가입.html "><span
								class="glyphicon glyphicon-user">회원가입</span></a></li>
						<li><a href="#"><span
								class="glyphicon glyphicon-shopping-cart">쇼핑카트</span></a></li>
						<li><a data-toggle="modal" href="#myModal"><span
								class="glyphicon glyphicon-user">내정보</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>


	<!--*****************************************************************nav-bar끝 *****************************************************************-->


	<!--********************************************************** 운동 화면 작업 시작 *****************************************************************-->


	<div class="row">
		<div>
			<!----------------------------------------------------------- 제품 라인 시작----------------------------------------------------------------------->
			<div class="container">

				<!-- 운동 tab 시작-->
				<div class="tab-content">
				
					<!-- 운동정보 tab 시작-->

						<!-- 1번 이미지-->
						<div class="col-md-4">
							<div class="thumbnail">

								<img src="image/test.jpg" alt="Lights"
									style="width: 40%; height: 40%">
								<div class="caption">
									<p>연습용 이미지입니다.</p>
								</div>

							</div>
						</div>
						<!-- 2번 이미지-->
						<div class="col-md-4">
							<div class="thumbnail">
								<img src="image/test.jpg" alt="Lights"
									style="width: 40%; height: 40%">
								<div class="caption">
									<p>연습용 이미지입니다.</p>
								</div>
							</div>
						</div>
						<!-- 3번 이미지-->
						<div class="col-md-4">
							<div class="thumbnail">
								<img src="image/test.jpg" alt="Lights"
									style="width: 40%; height: 40%">
								<div class="caption">
									<p>연습용 이미지입니다.</p>
								</div>
							</div>
						</div>
					

					<!-- 4번 이미지-->
					<div class="col-md-4">
						<div class="thumbnail">

							<img src="image/test.jpg" alt="Lights"
								style="width: 40%; height: 40%">
							<div class="caption">
								<p>연습용 이미지입니다.</p>
							</div>

						</div>
					</div>
					<!-- 5번 이미지-->
					<div class="col-md-4">
						<div class="thumbnail">
							<img src="image/test.jpg" alt="Lights"
								style="width: 40%; height: 40%">
							<div class="caption">
								<p>연습용 이미지입니다.</p>
							</div>
						</div>
					</div>
					<!-- 6번 이미지-->
					<div class="col-md-4">
						<div class="thumbnail">
							<img src="image/test.jpg" alt="Lights"
								style="width: 40%; height: 40%">
							<div class="caption">
								<p>연습용 이미지입니다.</p>
							</div>
						</div>
					</div>
				</div>
				<!-- 페이징 처리 -->
				<div style="float: right">
					<ul class="pagination">
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
					</ul>
				</div>
				<!-- 페이징 처리 끝 -->
				<button type="button" class="btn btn-default">등록하기</button>



			</div>

		</div>
		<!-- 운동정보 tab 끝-->

		<!-- 페이징 처리 시작 -->

	</div>

</body>

</html>
