package com.jky.Z_project.Controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jky.Z_project.Service.BoardService;
import com.jky.Z_project.VO.BoardVO;
import com.jky.Z_project.VO.PageParam;
import com.jky.Z_project.VO.PageVO;

import lombok.extern.java.Log;

@Controller
@Log
public class ExerciseMemoController {
	
	@Autowired
	BoardService boardService;
	//메인에서 운동메모란으로 이동하기.
	@RequestMapping("/insertMemoBoard")
	public ModelAndView enter(Model model) {
//		System.out.println(boardService.getBoard(1).toString());
	
		//한페이지에 나올 글의 숫자를 제한.	
		int limit=5;
		int page=1;
		//총 게시글수 구하기
		int totalPageCount=boardService.getAllBoards().size();
		
		//페이지 계산
		int maxPage =(int)((double)totalPageCount/limit+0.95);
		//현재 페이지에 보여줄 시작 페이지 수(1,11,21....)
		int startPage =(((int)((double)page/10+0.9))-1)*10+1;
		//현재 페이지에 보여줄 마지막 페이지 수(10,20,30....)
		int endPage = startPage+10-1;
		
		if(endPage>maxPage) endPage=maxPage;
		
		PageVO pageVo = new PageVO(); 
		
		pageVo.setEndPage(endPage);//
		pageVo.setListCount(totalPageCount);
		pageVo.setPage(page);
		pageVo.setStartPage(startPage);//
		pageVo.setMaxPage(maxPage);//
		
		ModelAndView model1=new ModelAndView();
		
//		model1.addObject("boardList", boardService.getAllBoards());
		model1.addObject("boardList", boardService.getAllBoardsByPaging(new PageParam(page, limit)));
		
		model1.setViewName("exerciseMemo");
		model1.addObject("pageInfo", pageVo);
		
		return model1;
		//return "exerciseMemo";
	}
	//쓰기 창 띄우기 컨트롤러
	@RequestMapping("/writingExerciseMemo")
	public String enter2(Model model) {
		log.info("/writingExerciseMemo");
		//model.addAttribute("boardVO", new BoardVO());
		return "writingExerciseMemo";
	}
	
	//실제 글 쓰기 컨트롤러
	@RequestMapping(value="/actionExerciseMemo",
				produces="text/html; charset=UTF-8",
				method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView write(BoardVO boardVO, RedirectAttributes ra) {
//	public String write(BoardVO boardVO) {
		System.out.println("/actionExerciseMemo");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/success");
		//ra.addFlashAttribute("boardVO", boardVO);
		//게시글 저장
		boardService.insertBoard(boardVO);
		System.out.println(boardVO);
		return mv;
	}
	@RequestMapping("/success")
	public String success(Model model) {
		log.info("success:" );
		model.addAttribute("msg", "게시글 저장에 성공했습니다");
		
		return "/success/success";
	}
	
	
	
}
