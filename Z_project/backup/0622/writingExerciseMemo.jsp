<%@ page contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<HTML lang="ko-kr">
<head>
<meta charset="utf-8" />
<title>무제 문서</title>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<!-- <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
 -->
 </head>

<body>
<!-- 전체장 시작--->
<div style="background-color:red; width:700px; height:300px; margin:0 auto;" >

<form:form commandName="boardVO" action="/Z_project/actionExerciseMemo" enctype="multipart/form-data" method="post">
<!-- 사용자 시작 -->
작성자 <input type="text" maxlength="10" id="id" name="id"/>

<!-- 사용자 끝 -->

<!-- 글제목 시작-->
<div style="background-color:yellow">
제목<input type="text" maxlength="100" style="width:500px; margin-left:10%" id="boardTitle" name="boardTitle"/>
</div>
<!--글제목 끝-->

<!-- 비밀번호 입력란 시작-->
<div style="float:right">
보안을 위해 비밀번호를 입력해주시길 바랍니다.
<label>비밀번호<input type="text" maxlength="10" id="boardPass" name="boardPass"/></label>
</div>
<!-- 비밀번호 입력란 끝-->

<!-- 내용란 시작-->
<div style=" margin-left:5%">
내용
<textarea rows="10" cols="80" id="boardContent" name="boardContent">
 </textarea>
</div>
<!-- 내용란 끝-->
<div>
날짜<input type="text" maxlength="10" id="boardDate" name="boardDate"/>

</div>

<!--  첨부파일 시작 -->
<div>
첨부파일<input type="file" maxlength="1000" id="boardFile" name="boardFile"/>
</div>
<!--  첨부파일 끝 -->

<input type="submit" id="submit" name="submit" value="글쓰기"  style="float: right" >
</form:form>

</div>
<!-- 전체장 끝--->
</body>
</html>
