--유저 테이블
CREATE TABLE user_tbl(
id VARCHAR2(10) primary key,
pw VARCHAR2(10) not null,
name VARCHAR2(10) not null,
address VARCHAR2(50) not null,
Email VARCHAR2(10) not null,
joindate Date,
memo VARCHAR2(100) not null
)
--확인용 데이터 삽입
Insert into user_tbl values(
'ruddydwls','rudyddwls','rudyddwls','rudyddwls','rudyddwls',null,'rudyddwls'
)

--제품 테이블
CREATE TABLE product_tbl(
productId VARCHAR2(10) ,
Product_Id VARCHAR2(10),
Product_Amount char,
Product_Price char
)

--운동 테이블
CREATE TABLE exercise_tbl(
	exerciseName varchar2(100),
	exerciseImgF varchar2(100),
	exerciseImgS varchar2(100),
	exerciseImgT varchar2(100),
	thumbNail varchar2(100),
	explain varchar2(100),
	exerciseUrl varchar2(1000)
)
alter table exercise_tbl modify explain varchar2(1000)
insert into exercise_tbl values (
'실험용 운동 이름 11', '실험용 1번 사진 주소11', '실험용 2번 사진 주소11', '실험용 3번 사진 주소11', '실험용 썸네일 사진 주소11', '실험용  설명11','실험용  url11'
)
SELECT * FROM exercise_tbl




--운동 기록 테이블
CREATE TABLE exerciserecord_tbl(
exerciseName varchar2(20),
exerciseDate VARCHAR2(21),
exerciseAmount char
)

--이벤트 테이블
CREATE TABLE event_tbl(
eventCode VARCHAR2(20) ,
Event_Name VARCHAR2(10),
Event_Date Date
)

--게시판 글 테이블
CREATE TABLE board_tbl(
boardNum int PRIMARY KEY,
boardKind varchar2(8),
boardTitle VARCHAR2(50),
boardContent VARCHAR2(1000),
boardPass VARCHAR2(20),
boardDate VARCHAR2(21),
boardFile VARCHAR2(200),
boardReadCount int default 0 not null,
id VARCHAR2(40)
)
--게시판 번호용 sequence 생성
create sequence board_seq increment by 1 start with 1;
SELECT * FROM board_tbl where boardNum=1;


drop table exercise_tbl
drop sequence board_seq


--확인용 데이터 삽입.
INSERT INTO board_tbl
		VALUES ( board_seq.nextval,'데이터제목11'
				,'데이터내용','12345678'
				,'데이터날짜','첨부파일', 0 ,'아이디');
SELECT * FROM board_tbl





--게시판 페이징 연습용.
SELECT * FROM ( 
            SELECT  
                   m.*, 
                   FLOOR((ROWNUM - 1)/2 +1) page  
            FROM ( 
                     SELECT * FROM board_tbl  
                        ORDER BY BOARDNUM DESC  
                 ) m 
          ) 
  WHERE page = 2;



