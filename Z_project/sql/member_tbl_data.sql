--------------------------------------------------------
--  파일이 생성됨 - 금요일-6월-22-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table BOARD_TBL
--------------------------------------------------------

  CREATE TABLE "ORACLEJAVA"."BOARD_TBL" 
   (	"BOARDNUM" NUMBER(*,0), 
	"BOARDTITLE" VARCHAR2(50 BYTE), 
	"BOARDCONTENT" VARCHAR2(1000 BYTE), 
	"BOARDPASS" VARCHAR2(20 BYTE), 
	"BOARDDATE" VARCHAR2(21 BYTE), 
	"BOARDFILE" VARCHAR2(200 BYTE), 
	"BOARDREADCOUNT" NUMBER(*,0) DEFAULT 0, 
	"ID" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into ORACLEJAVA.BOARD_TBL
SET DEFINE OFF;
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (1,'데이터제목1','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (2,'데이터제목2','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (3,'데이터제목3','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (4,'데이터제목4','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (5,'데이터제목5','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (6,'데이터제목6','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (7,'데이터제목7','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (21,'데이터제목8','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (22,'데이터제목9','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (23,'데이터제목10','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (24,'데이터제목10','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (25,'데이터제목11','데이터내용','12345678','데이터날짜','첨부파일',0,'아이디');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (29,'제목','연습','비밀','날짜','BitTorrent.exe',0,'이름');
Insert into ORACLEJAVA.BOARD_TBL (BOARDNUM,BOARDTITLE,BOARDCONTENT,BOARDPASS,BOARDDATE,BOARDFILE,BOARDREADCOUNT,ID) values (31,'나루토','원피스 ','보루토','일본만화','OperaSetup.exe',0,'츠쿠미');
--------------------------------------------------------
--  DDL for Index SYS_C007382
--------------------------------------------------------

  CREATE UNIQUE INDEX "ORACLEJAVA"."SYS_C007382" ON "ORACLEJAVA"."BOARD_TBL" ("BOARDNUM") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table BOARD_TBL
--------------------------------------------------------

  ALTER TABLE "ORACLEJAVA"."BOARD_TBL" ADD PRIMARY KEY ("BOARDNUM")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "ORACLEJAVA"."BOARD_TBL" MODIFY ("BOARDREADCOUNT" NOT NULL ENABLE);
